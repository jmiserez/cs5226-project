__author__ = 'lihou'

from Config import RANDOM_PREF
from ConnectionGenerator import getDBconnectionGenerator

from random import choice
from multiprocessing import Pool
from cx_Oracle import InterfaceError


class Worker(object):

    def __init__(self, wid, numIterations, pref, connectionGenerator):
        self._wid = wid
        self._numIterations = numIterations
        self._pref = pref
        self._dbConGen = connectionGenerator
        self._conn = None

    @property
    def id(self):
        return self._wid

    @property
    def pref(self):
        # return preference
        return self._pref

    @property
    def workload(self):
        # return iterations to run
        return self._numIterations

    @workload.setter
    def workload(self, workload):
        self._numIterations = workload

    def _getConnection(self):
        self._conn = self._dbConGen.getNewUserConnection()

    def execute(self, query):

        if self._conn is None:
            self._getConnection()
        try:
            self._conn.ping()
        except InterfaceError:
            self._getConnection()

        # execute query to stress database
        cur = self._conn.cursor()
        cur.execute(query)
        cur.fetchall()
        # print "worker {} finished, {} rows fetched.".format(self._wid, cur.rowcount)
        cur.close()

    def close(self):
        # close connection when done
        if self._conn:
            try:
                self._conn.ping()
                self._conn.close()
            except InterfaceError:
                # connection is not alive
                pass


class Simulator(object):
    # query simulator

    def __init__(self, numWorkers, numIterations, queryPool):
        self._numWorkers = numWorkers
        self._numIterations = numIterations
        self._queryPool = queryPool
        self._workers = []  # worker list
        self._defaultWorkerPref = RANDOM_PREF

    def initWorkers(self):
        # initialize all the workers
        print "Initializing workers"
        self._workers = []
        for i in xrange(self._numWorkers):
            connGen = getDBconnectionGenerator()
            worker = Worker(i, self._numIterations, self._defaultWorkerPref, connGen)
            print "Worker {:2d} initialized".format(i)
            self._workers.append(worker)

    def _areWorkersInitialized(self):
        # check if all workers were initialized
        if all([isinstance(w, Worker) for w in self._workers]) and \
                len(self._workers) == self._numWorkers:
            return True
        else:
            return False

    def simulate(self):
        # run the simulation
        if not self._areWorkersInitialized():
            self.initWorkers()

        print "Simulation begins ... "
        pool = Pool(self._numWorkers)
        args = [(worker, self._queryPool) for worker in self._workers]
        res = pool.map_async(_runWork, args)
        for r in res.get():
            print r

        pool.close()
        pool.join()
        print "Simulation done."


# due to the reason instancemethod could not be pickled
def _runWork((worker, queryPool)):
    # run individual worker job
    for i in xrange(worker.workload):
        query = chooseQuery(worker, queryPool)
        worker.execute(query)
    worker.close()
    return "worker #{:2d} completes {:2d} runs".format(worker.id, worker.workload)


def chooseQuery(worker, queryPool):
    # choose query from pool according to the preference of worker
    if not queryPool:
        raise ValueError("empty query pool")

    queryType = worker.pref if worker.pref != RANDOM_PREF else choice(queryPool.keys())
    return choice(queryPool[queryType])


if __name__ == '__main__':

    from QueryPool import getQueryPool

    s = Simulator(10, 10, getQueryPool())

    s.initWorkers()
    s.simulate()
