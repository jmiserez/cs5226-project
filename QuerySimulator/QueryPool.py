__author__ = 'lihou'

# queries
# sorting memory related
SORT_RELATED_QUERIES = ['SELECT * FROM COUNTRIES ORDER BY COUNTRY_ID',
                        'SELECT * FROM COUNTRIES ORDER BY COUNTRY_NAME']

# buffer cache related
BUFFER_RELATED_QUERIES = ['SELECT * FROM JOBS, EMPLOYEES']

queryPool = {'sort': SORT_RELATED_QUERIES, 
              'buffer_cache': BUFFER_RELATED_QUERIES}


def getQueryPool():
    return queryPool
