__author__ = 'lihou'

import cx_Oracle as cx
from Config import SUSER, SUSER_PASSWORD, VUSER, VUSER_PASSWORD, TNS


class _dbConnectionGenerator(object):

    def __init__(self, suser, spasswd, vuser, vpasswd, tns):
        self._suser = suser
        self._spasswd = spasswd
        self._vuser = vuser
        self._vpasswd = vpasswd
        self._tns = tns

    def getNewUserConnection(self):
        """
        get a new connection of user account
        """
        return cx.connect(self._vuser, self._vpasswd, self._tns)

    def getNewSystemConnection(self):
        """
        get a new connection of by system account
        """
        return cx.connect(self._suser, self._spasswd, self._tns)


def getDBconnectionGenerator():
    """
    return a db connection generator 
    """
    return _dbConnectionGenerator(SUSER, SUSER_PASSWORD, VUSER, VUSER_PASSWORD, TNS)


