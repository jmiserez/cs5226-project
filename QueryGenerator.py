#!/usr/bin/python
#FileName: QueryGenerator.py

T_COUNTRIES_RNUM = 25
T_DEPARTMENTS_RNUM = 27
T_EMPLOYEES_RNUM = 107
T_JOBS_RNUM = 19
T_JOB_HISTORY_RNUM = 10
T_LOCATIONS_RNUM = 23
T_REGIONS_RNUM = 4 

table_rownums = [T_COUNTRIES_RNUM, T_DEPARTMENTS_RNUM, T_EMPLOYEES_RNUM, T_JOBS_RNUM, T_JOB_HISTORY_RNUM, T_LOCATIONS_RNUM, T_REGIONS_RNUM]

T_COUNTRIES_NAME = 'COUNTRIES'
T_DEPARTMENTS_NAME = 'DEPARTMENTS'
T_EMPLOYEES_NAME = 'EMPLOYEES'
T_JOBS_NAME = 'JOBS'
T_JOB_HISTORY_NAME = 'JOB_HISTORY'
T_LOCATIONS_NAME = 'LOCATIONS'
T_REGIONS_NAME = 'REGIONS'

table_names = [T_COUNTRIES_NAME, T_DEPARTMENTS_NAME, T_EMPLOYEES_NAME, T_JOBS_NAME, T_JOB_HISTORY_NAME, T_LOCATIONS_NAME, T_REGIONS_NAME]

#library cache realted queries
LIBRARYCACHE_RELATED_QURIES = []
HARD_PARSE_RELATED_QURIES = []
REDO_BUFFER_RELATED_INSERT = []
REDO_BUFFER_RELATED_DELETE = []
MEM_SORTS_RELATED_QURIES = []

for names, nums in zip(table_names, table_rownums):
	for i in range(0, nums):
		LIBRARYCACHE_RELATED_QURIES.append('SELECT * FROM ' + names + ' WHERE ROWNUM <=' + str(i + 1))

#For debug
#for i in range(0, len(LIBRARYCACHE_RELATED_QURIES)):
#	print LIBRARYCACHE_RELATED_QURIES[i]

#for the redo buffer query, need to add a commit in the simulator
#to avoid reference conflict, only manipulate one table countries

#format: insert into COUNTRIES values ('a2', 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX', 1); 

temp_country_name = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
#INSERT
for c_id in range(0, ord('Z') - ord('A') + 1):
	for i in range(0,10):
		query = ('INSERT INTO ' + T_COUNTRIES_NAME + ' VALUES ' + '(\'' + chr(c_id + ord('A')) + str(i) + '\', \'' + temp_country_name + '\', 1)')
		REDO_BUFFER_RELATED_INSERT.append(query)
		query = ('INSERT INTO ' + T_COUNTRIES_NAME + ' VALUES ' + '(\'' + chr(c_id + ord('a')) + str(i) + '\', \'' + temp_country_name + '\', 2)')
		REDO_BUFFER_RELATED_INSERT.append(query)

#DELETE
for c_id in range(0, ord('Z') - ord('A') + 1):
	for i in range(0,10):
		query = ('DELETE FROM ' + T_COUNTRIES_NAME + ' WHERE COUNTRY_ID = \'' +  chr(c_id + ord('A')) + str(i) + '\'')
		REDO_BUFFER_RELATED_DELETE.append(query)
		query = ('DELETE FROM ' + T_COUNTRIES_NAME + ' WHERE COUNTRY_ID = \'' +  chr(c_id + ord('a')) + str(i) + '\'')
		REDO_BUFFER_RELATED_DELETE.append(query)

#for debug
#for i in range(0, len(REDO_BUFFER_RELATED_QURIES)):
#	print REDO_BUFFER_RELATED_QURIES[i]

#print len(REDO_BUFFER_RELATED_QURIES)


#SORT 
