import sqlite3
import time
import random
import datetime

jobid = 1
inter_days = 0
inter_seconds = 15
inter_minutes = 0
inter_hours = 0


t_library_cache           = {'name':'LIBRARY_CACHE',           'baserate':0.95 , 'basevalue':1000 , 'min':0.90 , 'max':1.0 }
#t_hard_soft_parse         = {'name':'HARD_SOFT_PARSE',         'baserate': ,     'min': , 'max': }
#t_row_cache               = {'name':'ROW_CACHE',               'base': ,     'min': , 'max': }
#t_shared_pool_gross_usage = {'name':'SHARED_POOL_GROSS_USAGE', 'base': ,     'min': , 'max': }
#t_buffer_pool_hit_ratio   = {'name':'BUFFER_POOL_HIT_RATIO',   'base': ,     'min': , 'max': }
#t_redo_log_stat           = {'name':'REDO_LOG_STAT',           'base': ,     'min': , 'max': }
#t_soft_mem_stat           = {'name':'SORT_MEM_STAT',           'base': ,     'min': , 'max': }


#class datetime.timedelta([days[, seconds[, microseconds[, milliseconds[, minutes[, hours[, weeks]]]]]]])
def setInterval(d, s, m, h):
	return datetime.timedelta(days = d , seconds = s, minutes = m, hours = h)

def getTotalSeconds(td):
	return (td.seconds + td.days * 24 * 3600)

#def library_cache_insert(cursor, tablename, value):
#		cursor.execute('INSERT INTO ' + tablename + ' VALUES (?, ?, ?, ?, ?)', value)

def insert_library_cache(cursor, table, job_id, start_time, end_time, interval):
	totalseconds = getTotalSeconds(interval)
	range_num = getTotalSeconds(end_time - start_time) / totalseconds
	
	current_time = start_time - interval
	pins_sum = table['basevalue'] - totalseconds
	pinhits_sum = int((table['basevalue'] - totalseconds) * (table['baserate'] - 0.1))

	for i in range(1, range_num + 3):
		pins_add = int(random.uniform(table['min'], table['max']) * totalseconds)
		current_time = current_time + interval
		pins_sum = pins_sum + pins_add
		pinhits_sum = random.randint(int(random.uniform(table['min'], table['max']) * pins_add) + pinhits_sum, pinhits_sum + pins_add)
		if current_time > end_time:
			print str(current_time)
			break	
		else:
			cursor.execute('INSERT INTO ' + table['name'] + ' VALUES (?, ?, ?, ?, ?)', (i, job_id, str(current_time), pins_sum, pinhits_sum))
			print float(pinhits_sum) / float(pins_sum)

#can set or get the current time 
interval = setInterval(inter_days, inter_seconds, inter_minutes, inter_hours)
print str(interval)

#starttime = datetime.datetime(2013, 4, 17, 9, 0, 0)
#starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
starttime = datetime.datetime.now().replace(microsecond = 0)
print str(starttime)
#endtime = datetime.datetime(2013, 4, 17, 23, 59, 59)
#endtime = starttime.replace(hour = 23, minute = 59, second = 59)
#endtime = starttime.replace(hour = 23, minute = 59, second = 59)
#endtime = starttime + 10 * interval
endtime = starttime + interval
print str(endtime)


#db part
conn = sqlite3.connect('example.db')

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS LIBRARY_CACHE (
  ID                          INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID                      INTEGER,
  UPDATE_TIME                 TEXT,
  PINS_SUM                    INTEGER,
  PINHITS_SUM                 INTEGER
)''')

insert_library_cache(c, t_library_cache, jobid, starttime, endtime, interval)
# Insert a row of data
#c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")


c.execute('''CREATE TABLE IF NOT EXISTS HARD_SOFT_PARSE (
  ID                  INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID              INTEGER,
  UPDATE_TIME         TEXT,
  PARSE_COUNT_TOTAL   INTEGER,
  PARSE_COUNT_HARD    INTEGER
)''')

c.execute('''CREATE TABLE IF NOT EXISTS ROW_CACHE (
  ID              INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID          INTEGER,
  UPDATE_TIME     TEXT,
  FIXED_SUM       INTEGER,
  GETS_SUM        INTEGER,
  GETMISSES_SUM   INTEGER
)''')

c.execute('''CREATE TABLE IF NOT EXISTS SHARED_POOL_GROSS_USAGE (
  ID                  INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID              INTEGER,
  UPDATE_TIME         TEXT,
  SHARED_POOL_USED  INTEGER,
  SHARED_POOL_SIZE    INTEGER
)''')

#BUFFER CACHE
c.execute('''CREATE TABLE IF NOT EXISTS BUFFER_POOL_HIT_RATIO (
  ID                          INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID                      INTEGER,
  UPDATE_TIME                 TEXT,
  DB_BLOCK_GETS_FROM_CACHE    INTEGER,
  CONSISTENT_GETS_FROM_CACHE  INTEGER,
  PHYSICAL_READS_CACHE        INTEGER
)''')

#REDO LOG
c.execute('''CREATE TABLE IF NOT EXISTS REDO_LOG_STAT (
  ID                              INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID                          INTEGER,
  UPDATE_TIME                     TEXT,
  REDO_ENTRIES                    INTEGER,
  REDO_BUFFER_ALLOCATION_RETRIES  INTEGER,
  LOG_FILESIZE                    INTEGER,
  OPTIMAL_LOGFILE_SIZE            INTEGER
)''')

#SORTS AREA
c.execute('''CREATE TABLE IF NOT EXISTS SORTS_MEM_STAT (
  ID                              INTEGER PRIMARY KEY AUTOINCREMENT,
  JOB_ID                          INTEGER,
  UPDATE_TIME                     TEXT,
  SORT_AREA_SIZE                  INTEGER,
  SORT_AREA_RETAINED_SIZE         INTEGER,
  SORTS_MEMORY                    INTEGER,
  SORTS_DISK                      INTEGER
)''')


# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()

