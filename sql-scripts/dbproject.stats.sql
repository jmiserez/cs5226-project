CREATE SEQUENCE seq_stats
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 10;

CREATE TABLE dbproject.stats
(
id int NOT NULL,
timestamp date NOT NULL,
database varchar(255),
shared_pool_bytes_total int,
shared_pool_bytes_free int,
PRIMARY KEY (id)
);
