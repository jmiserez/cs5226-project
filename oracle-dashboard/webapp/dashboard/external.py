import subprocess
import os
import signal
from defaults import *

def executeShellCmd(cmd, input=None, chdir='.', nowait=False):
    my_env = os.environ.copy()
    oracle_home= os.path.abspath(RELATIVE_ORACLE_HOME)
    oracle_home_lib= os.path.abspath(RELATIVE_ORACLE_HOME+'/lib')
    oracle_home_bin= os.path.abspath(RELATIVE_ORACLE_HOME+'/bin')
    ld_library_path= os.path.abspath(RELATIVE_LD_LIBRARY_PATH)
    my_env['ORACLE_HOME'] = oracle_home
    if 'LD_LIBRARY_PATH' in my_env:        
         my_env['LD_LIBRARY_PATH'] = oracle_home + ':' + oracle_home_lib + ':' + oracle_home_bin + ':' + ld_library_path + ':' + my_env['LD_LIBRARY_PATH']
    else: 
        my_env['LD_LIBRARY_PATH'] = oracle_home + ':' + oracle_home_lib + ':' + oracle_home_bin + ':' + ld_library_path
    if 'PATH' in my_env:        
         my_env['PATH'] = oracle_home + ':' + oracle_home_lib + ':' + oracle_home_bin + ':' + ld_library_path + my_env['PATH']
    else: 
        my_env['PATH'] = oracle_home + ':' + oracle_home_lib + ':' + oracle_home_bin + ':' + ld_library_path
    print " ".join(map(str,cmd))
    p = subprocess.Popen(cmd, close_fds=True, cwd=chdir, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE, env=my_env)
    if nowait == True:
        return []
    else:
        if input is not None:
            stdoutdata = p.communicate(input)[0];
        else:
            stdoutdata = p.communicate()[0];
        if stdoutdata is not None:
            stdoutlines = stdoutdata.splitlines()
        else:
            stdoutlines = []
        lines = stdoutlines
        retval = p.wait()
        print "\n".join(map(str,lines))
        return lines

def checkConnection(sqlplusTnsString):
    sqlcmd = """select 1 from dual;"""
    cmd = ['sqlplus', '-s', '-l', '-r' ,'3', sqlplusTnsString]
    result = executeShellCmd(cmd, sqlcmd)
    for line in result:
        if line.strip() == "1":
            return True
    return False

def executeRemote(sqlplusTnsString, sqlcmd):
    cmd = ['sqlplus', '-s', '-l', '-r' ,'3', sqlplusTnsString]
    result = executeShellCmd(cmd, sqlcmd)
    return "\n".join(map(str,result))

def startJob(jobid, user, passwd, tns):
    cmd = [os.path.abspath(RELATIVE_PYTHON_PATH), CONTROLLER_FILENAME, '--start', str(jobid), user, passwd, tns, str(CONTROLLER_INTERVAL)]
    result = executeShellCmd(cmd, chdir=os.path.abspath(CONTROLLER_PATH), nowait=True)
    
def stopJob(jobid):
    cmd = [os.path.abspath(RELATIVE_PYTHON_PATH), CONTROLLER_FILENAME, '--stop', str(jobid)]
    result = executeShellCmd(cmd, chdir=os.path.abspath(CONTROLLER_PATH), nowait=True)

def listJobs():
    cmd = [os.path.abspath(RELATIVE_PYTHON_PATH), CONTROLLER_FILENAME, '--machinereadable', '--list']
    result = executeShellCmd(cmd, chdir=os.path.abspath(CONTROLLER_PATH))
    return [int(i) for i in result]

    