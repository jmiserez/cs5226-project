# External tools
SQLPLUS_PATH = './lib/instantclient'
SQLPLUS_FILENAME = 'sqlplus'
CONTROLLER_PATH = './backend'
CONTROLLER_FILENAME = 'Controller.py'
CONTROLLER_INTERVAL = '30'
RELATIVE_LD_LIBRARY_PATH = './lib/instantclient/lib'
RELATIVE_ORACLE_HOME = './lib/instantclient'
RELATIVE_PYTHON_PATH = '../venv/bin/python'

# Intervals
SHORT_INTERVAL = 2
LONG_INTERVAL = 10

# Thresholds [yellow, red]
LIBRARY_CACHE_MISS_RATIO_HIGH = [0.05, 0.10] #large -> problem
DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH = [0.005, 0.01] #large -> problem
SHARED_POOL_GROSS_USAGE_HIGH = [0.90, 0.95] #large -> problem
HARD_PARSE_RATIO_HIGH = [0.05, 0.10] #large -> problem
# buffer cache
BUFFER_CACHE_HIT_RATIO_LOW = [0.98, 0.95] #small -> problem
# redo log
REDO_LOG_BUFFER_LARGE = [0.55, 0.50] #small -> problem
REDO_LOG_BUFFER_SMALL = [0.95, 0.99] #large -> problem
# sort memory
SORT_AREA_LARGE = [0.06, 0.05] #small -> problem
SORT_AREA_SMALL = [0.09, 0.10] #large -> problem

SQL_ELAPSED_SECONDS = [5,6] #second

#Texts
TEXT_LIBRARY_CACHE_MISS_RATIO_HIGH = ['Shared Pool', 'Library cache miss ratio']
TEXT_DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH = ['Shared Pool', 'Data dictionary cache miss ratio']
TEXT_SHARED_POOL_GROSS_USAGE_HIGH = ['Shared Pool', 'Gross usage']
TEXT_HARD_PARSE_RATIO_HIGH = ['Shared Pool', 'Hard parse ratio']
# buffer cache
TEXT_BUFFER_CACHE_HIT_RATIO_LOW = ['Buffer Cache', 'Hit ratio']
# redo log
TEXT_REDO_LOG_BUFFER_LARGE = ['Redo Log', 'Buffer entries hit ratio (lower threshold)']
TEXT_REDO_LOG_BUFFER_SMALL = ['Redo Log', 'Buffer entries hit ratio (upper threshold)']
# sort memory
TEXT_SORT_AREA_LARGE = ['Sort Area', 'Sort disk ratio (lower threshold)']
TEXT_SORT_AREA_SMALL = ['Sort Area', 'Sort disk ratio (upper threshold)']

TEXT_SQL_ELAPSED_SECONDS = ['SQL Query', 'Elapsed seconds']

#Texts for Charts
TEXT_LIBRARY_CACHE_HIT_RATIO = ['Shared Pool', 'Library cache hit ratio']
TEXT_ROW_CACHE_HIT_RATIO = ['Shared Pool', 'Data dictionary cache hit ratio']
TEXT_SHARED_POOL_GROSS_USAGE_PCT = ['Shared Pool', 'Gross usage']
TEXT_HARD_PARSE_RATIO = ['Shared Pool', 'Hard parse ratio']
TEXT_BUFFER_CACHE_HIT_RATIO = ['Buffer Cache', 'Hit ratio']
TEXT_REDO_BUFFER_HIT_RATIO = ['Redo Log', 'Buffer entries hit ratio']
TEXT_SORTS_DISK_RATIO = ['Sort Area', 'Memory Sorts ratio (sorts(disk)/sorts(disk)+sorts(memory))']
