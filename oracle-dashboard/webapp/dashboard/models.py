# Create your models here.
from django.db import models
from django.template.defaultfilters import default

import defaults

class Dashboard(models.Model):
    name = models.TextField()
    host = models.TextField()
    port = models.IntegerField()
    sid = models.TextField()
    username = models.TextField()
    password = models.TextField()
    
    active = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    
    shortIntervalMinutes = models.IntegerField(default=defaults.SHORT_INTERVAL)
    longIntervalMinutes = models.IntegerField(default=defaults.LONG_INTERVAL)
    
    yellowLibraryCacheMissRatioHighPercent = models.FloatField(default=defaults.LIBRARY_CACHE_MISS_RATIO_HIGH[0])
    yellowDataDictionaryCacheMissRatioHighPercent = models.FloatField(default=defaults.DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH[0])
    yellowSharedPoolGrossUsageHighPercent = models.FloatField(default=defaults.SHARED_POOL_GROSS_USAGE_HIGH[0])
    yellowHardParseRatioHighPercent = models.FloatField(default=defaults.HARD_PARSE_RATIO_HIGH[0])
    yellowBufferCacheHitRatioLowPercent = models.FloatField(default=defaults.BUFFER_CACHE_HIT_RATIO_LOW[0])
    yellowRedoLogBufferSmallPercent = models.FloatField(default=defaults.REDO_LOG_BUFFER_SMALL[0])
    yellowRedoLogBufferLargePercent = models.FloatField(default=defaults.REDO_LOG_BUFFER_LARGE[0])
    yellowSortAreaSmallPercent = models.FloatField(default=defaults.SORT_AREA_SMALL[0])
    yellowSortAreaLargePercent = models.FloatField(default=defaults.SORT_AREA_LARGE[0])
    yellowSqlElapsedSeconds = models.FloatField(default=defaults.SQL_ELAPSED_SECONDS[0])
    
    redLibraryCacheMissRatioHighPercent = models.FloatField(default=defaults.LIBRARY_CACHE_MISS_RATIO_HIGH[1])
    redDataDictionaryCacheMissRatioHighPercent = models.FloatField(default=defaults.DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH[1])
    redSharedPoolGrossUsageHighPercent = models.FloatField(default=defaults.SHARED_POOL_GROSS_USAGE_HIGH[1])
    redHardParseRatioHighPercent = models.FloatField(default=defaults.HARD_PARSE_RATIO_HIGH[1])
    redBufferCacheHitRatioLowPercent = models.FloatField(default=defaults.BUFFER_CACHE_HIT_RATIO_LOW[1])
    redRedoLogBufferSmallPercent = models.FloatField(default=defaults.REDO_LOG_BUFFER_SMALL[1])
    redRedoLogBufferLargePercent = models.FloatField(default=defaults.REDO_LOG_BUFFER_LARGE[1])
    redSortAreaSmallPercent = models.FloatField(default=defaults.SORT_AREA_SMALL[1])
    redSortAreaLargePercent = models.FloatField(default=defaults.SORT_AREA_LARGE[1])
    redSqlElapsedSeconds = models.FloatField(default=defaults.SQL_ELAPSED_SECONDS[1])
    
    def __unicode__(self):
        return self.name + ' (' + self.username + '@//' + self.host + ':' + str(self.port) + '/' + '' + self.sid + ')'
    
    def tnsHumanReadableString(self):
        return self.username + '@//' + self.host + ':' + str(self.port) + '/' + '' + self.sid
    
    def tnsSqlPlusString(self):
        return self.username + '/' + self.password + '@' + self.makeDsn()

    def makeDsn(self):
        return '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=' + self.host + ')(Port=' + str(self.port) + '))(CONNECT_DATA=(SID=' + self.sid + ')))'

class SqlHistory(models.Model):
    dashboard = models.ForeignKey(Dashboard)
    cmd = models.TextField()
    answer = models.TextField()

    