from django import forms
from django.forms.fields import CharField, IntegerField, DateField
from django.forms.widgets import TextInput, DateTimeInput

from models import *
from django.forms.util import flatatt
from django.utils.html import format_html
from django.utils.encoding import force_text
from defaults import *

class AddDashboardForm(forms.Form):
    name = forms.CharField(label='Dashboard name', widget=forms.TextInput(attrs={'placeholder': 'Name for this dashboard'}))
    host = forms.CharField(label='Host', widget=forms.TextInput(attrs={'placeholder': 'Hostname or IP address'}))
    port = forms.IntegerField(label='Port', widget=forms.TextInput(attrs={'placeholder': 'Port in range 0-65535'}))
    sid = forms.CharField(label='Database', widget=forms.TextInput(attrs={'placeholder': 'Oracle SID'}))
    username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'placeholder': 'Database user'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'placeholder': 'Password for database user'}))

class ChangeStateForm(forms.Form):
    pass # all fields are special and handled manually in views.py

class SettingsTextInput(forms.TextInput):
    append = ''
    prepend = ''
    prependid = ''
    appendclasses = ''

    def __init__(self, attrs=None):
        if attrs is not None:
            self.append = attrs.pop('append', self.input_type)
            self.prepend = attrs.pop('prepend', self.input_type)
            self.prependid = attrs.pop('prependid', self.input_type)
            self.appendclasses = attrs.pop('appendclasses', self.input_type)
        super(SettingsTextInput, self).__init__(attrs)
        
    def render(self, name, value, attrs=None):
        self.input_html = super(SettingsTextInput, self).render(name, value, attrs)
        self.prepend_html = " "
        self.append_html = " "
        self.input_classes = " "
        if self.prepend != '':
             self.prepend_html = '<span class="add-on">'+self.prepend+'</span>'
             self.input_classes = self.input_classes + ' input-prepend'
        if self.append != '':
             self.append_html = '<span class="add-on">'+self.append+'</span>'
             self.input_classes = self.input_classes + ' input-append'
             if self.appendclasses != '':
                 self.input_classes = self.input_classes + ' ' + self.appendclasses
        if self.prependid != '':
            return '<div id="'+ self.prependid +'" class="' + self.input_classes + '">' + self.prepend_html + self.input_html + self.append_html + '</div>'
        else:
            return '<div class="' + self.input_classes + '">' + self.prepend_html + self.input_html + self.append_html + '</div>'

class SettingsDateTimeInput(forms.DateTimeInput):
    append = ''
    prepend = ''
    prependid = ''
    appendclasses = ''

    def __init__(self, attrs=None):
        if attrs is not None:
            self.append = attrs.pop('append', self.input_type)
            self.prepend = attrs.pop('prepend', self.input_type)
            self.prependid = attrs.pop('prependid', self.input_type)
            self.appendclasses = attrs.pop('appendclasses', self.input_type)
        super(SettingsDateTimeInput, self).__init__(attrs)
        
    def render(self, name, value, attrs=None):
        self.input_html = super(SettingsDateTimeInput, self).render(name, value, attrs)
        self.prepend_html = " "
        self.append_html = " "
        self.input_classes = " "
        if self.prepend != '':
             self.prepend_html = '<span class="add-on">'+self.prepend+'</span>'
             self.input_classes = self.input_classes + ' input-prepend'
        if self.append != '':
             self.append_html = '<span class="add-on">'+self.append+'</span>'
             self.input_classes = self.input_classes + ' input-append'
             if self.appendclasses != '':
                 self.input_classes = self.input_classes + ' ' + self.appendclasses
        if self.prependid != '':
            return '<div id="'+ self.prependid +'" class="' + self.input_classes + '">' + self.prepend_html + self.input_html + self.append_html + '</div>'
        else:
            return '<div class="' + self.input_classes + '">' + self.prepend_html + self.input_html + self.append_html + '</div>'

class SettingsForm(forms.Form):
    
    warnHighAttrs =    {'append': '%', 'prepend' : '<span class="label label-warning">Warning above</span>', 'class' : 'input-mini text-right'}
    warnLowAttrs =     {'append': '%', 'prepend' : '<span class="label label-warning">Warning below</span>', 'class' : 'input-mini text-right'}
    seriousHighAttrs = {'append': '%', 'prepend' : '<span class="label label-important">Needs attention above</span>', 'class' : 'input-mini text-right'}
    seriousLowAttrs =  {'append': '%', 'prepend' : '<span class="label label-important">Needs attention below</span>', 'class' : 'input-mini text-right'}
    
    shortIntervalMinutes = forms.IntegerField(min_value=1, label='Short time interval', widget=SettingsTextInput(attrs={'prepend' : '', 'append': 'minutes', 'class' : 'input-medium text-right'}))
    longIntervalMinutes = forms.IntegerField(min_value=1, label='Long time interval', widget=SettingsTextInput(attrs={'prepend' : '', 'append': 'minutes', 'class' : 'input-medium text-right'}))


    yellowLibraryCacheMissRatioHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_LIBRARY_CACHE_MISS_RATIO_HIGH[0]+': '+TEXT_LIBRARY_CACHE_MISS_RATIO_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redLibraryCacheMissRatioHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_LIBRARY_CACHE_MISS_RATIO_HIGH[0]+': '+TEXT_LIBRARY_CACHE_MISS_RATIO_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    yellowDataDictionaryCacheMissRatioHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH[0]+': '+TEXT_DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redDataDictionaryCacheMissRatioHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH[0]+': '+TEXT_DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    yellowSharedPoolGrossUsageHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_SHARED_POOL_GROSS_USAGE_HIGH[0]+': '+TEXT_SHARED_POOL_GROSS_USAGE_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redSharedPoolGrossUsageHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_SHARED_POOL_GROSS_USAGE_HIGH[0]+': '+TEXT_SHARED_POOL_GROSS_USAGE_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    yellowHardParseRatioHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_HARD_PARSE_RATIO_HIGH[0]+': '+TEXT_HARD_PARSE_RATIO_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redHardParseRatioHighPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_HARD_PARSE_RATIO_HIGH[0]+': '+TEXT_HARD_PARSE_RATIO_HIGH[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    yellowBufferCacheHitRatioLowPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_BUFFER_CACHE_HIT_RATIO_LOW[0]+': '+TEXT_BUFFER_CACHE_HIT_RATIO_LOW[1], 
                                                              widget=SettingsTextInput(attrs=warnLowAttrs.copy()))
    redBufferCacheHitRatioLowPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_BUFFER_CACHE_HIT_RATIO_LOW[0]+': '+TEXT_BUFFER_CACHE_HIT_RATIO_LOW[1], 
                                                              widget=SettingsTextInput(attrs=seriousLowAttrs.copy()))
    yellowRedoLogBufferSmallPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_REDO_LOG_BUFFER_SMALL[0]+': '+TEXT_REDO_LOG_BUFFER_SMALL[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redRedoLogBufferSmallPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_REDO_LOG_BUFFER_SMALL[0]+': '+TEXT_REDO_LOG_BUFFER_SMALL[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    yellowRedoLogBufferLargePercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_REDO_LOG_BUFFER_LARGE[0]+': '+TEXT_REDO_LOG_BUFFER_LARGE[1], 
                                                              widget=SettingsTextInput(attrs=warnLowAttrs.copy()))
    redRedoLogBufferLargePercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_REDO_LOG_BUFFER_LARGE[0]+': '+TEXT_REDO_LOG_BUFFER_LARGE[1], 
                                                              widget=SettingsTextInput(attrs=seriousLowAttrs.copy()))
    yellowSortAreaSmallPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_SORT_AREA_SMALL[0]+': '+TEXT_SORT_AREA_SMALL[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redSortAreaSmallPercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_SORT_AREA_SMALL[0]+': '+TEXT_SORT_AREA_SMALL[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    yellowSortAreaLargePercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_SORT_AREA_LARGE[0]+': '+TEXT_SORT_AREA_LARGE[1], 
                                                              widget=SettingsTextInput(attrs=warnLowAttrs.copy()))
    redSortAreaLargePercent = forms.FloatField(min_value=0.0, max_value=100.0, 
                                                              label='Threshold for '+TEXT_SORT_AREA_LARGE[0]+': '+TEXT_SORT_AREA_LARGE[1], 
                                                              widget=SettingsTextInput(attrs=seriousLowAttrs.copy()))
    yellowSqlElapsedSeconds = forms.FloatField(min_value=0.0, 
                                                              label='Threshold for '+TEXT_SQL_ELAPSED_SECONDS[0]+': '+TEXT_SQL_ELAPSED_SECONDS[1], 
                                                              widget=SettingsTextInput(attrs=warnHighAttrs.copy()))
    redSqlElapsedSeconds = forms.FloatField(min_value=0.0, 
                                                              label='Threshold for '+TEXT_SQL_ELAPSED_SECONDS[0]+': '+TEXT_SQL_ELAPSED_SECONDS[1], 
                                                              widget=SettingsTextInput(attrs=seriousHighAttrs.copy()))
    
class OnDemandForm(forms.Form):
    short = forms.IntegerField(min_value=1, label='Short time interval', widget=SettingsTextInput(attrs={'prepend' : '', 'append': 'minutes', 'class' : 'input-medium text-right'}))
    long = forms.IntegerField(min_value=1, label='Long time interval', widget=SettingsTextInput(attrs={'prepend' : '', 'append': 'minutes', 'class' : 'input-medium text-right'}))
    start = forms.DateTimeField(label='Start date', widget=SettingsDateTimeInput(attrs={'prepend' : '', 'append': '<i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar"></i>', 'class' : 'input-medium text-right', 'prependid' : 'datetimepicker1', 'appendclasses' : 'date',  'data-format' : 'yyyy-MM-dd HH:mm:ss', 'placeholder': 'Start of job'}))
    end = forms.DateTimeField(label='End date', widget=SettingsDateTimeInput(attrs={'prepend' : '', 'append': '<i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar"></i>', 'class' : 'input-medium text-right', 'prependid' : 'datetimepicker2', 'appendclasses' : 'date', 'data-format' : 'yyyy-MM-dd HH:mm:ss', 'placeholder': 'End of job'}))
