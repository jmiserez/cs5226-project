from django.conf.urls import patterns, include, url
from dashboard import views

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    url(r'^home', views.home, name='home'),
    url(r'^about', views.about, name='about'),
    url(r'^add', views.add_dashboard, name='add_dashboard'),
    url(r'^check$', views.check_connection, name='check_connection'),
    url(r'^(?P<dashboard_id>\d+)$', views.view_dashboard, name='view_dashboard'),
    url(r'^(?P<dashboard_id>\d+)/$', views.redirect_to_view_dashboard, name='view_dashboard'),
    url(r'^(?P<dashboard_id>\d+)/change_state', views.change_state, name='change_state'),
    url(r'^(?P<dashboard_id>\d+)/save_settings', views.save_settings, name='save_settings'),
    url(r'^(?P<dashboard_id>\d+)/generate_ondemand', views.generate_ondemand, name='generate_ondemand'),
    url(r'^(?P<dashboard_id>\d+)/execute_remote', views.execute_remote, name='execute_remote'),
    url(r'^(?P<dashboard_id>\d+)/check', views.check_connection2, name='check_connection2'),
    url(r'^stats/$', views.get_stats),
    url(r'^overview/$', views.overview),
    url(r'^latest-stats/$', views.get_latest_stats),
)
