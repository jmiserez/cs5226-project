from models import *
from forms import *
import defaults
from backend.Advisor import Advisor, ProblemIdentifier, StatsIdentifier
from datetime import datetime, timedelta
import time
from backend.Config import SQLITE_DATETIME_FORMAT


def advisor_available(dashboard_id):
    advisorAvailable = True
    try:
        with Advisor(dashboard_id) as adv:
            pass
    except ValueError:
        advisorAvailable = False
    
    return advisorAvailable

def get_low_sql(dashboard_id):
    if advisor_available(dashboard_id) == False:
        return []
    else:
        with Advisor(dashboard_id) as adr:
            lis = adr.GetLowEfficiencySql(datetime.now())
            return lis

def get_long_sql(dashboard_id):
    if advisor_available(dashboard_id) == False:
        return []
    else:
        with Advisor(dashboard_id) as adr:
            lis = adr.GetLongRunSql(datetime.now())
            return lis

def cpu_intensive_session(dashboard_id):
    if advisor_available(dashboard_id) == False:
        return []
    else:
        with Advisor(dashboard_id) as adr:
            lis = adr.GetCPUIntensiveSession(datetime.now())
            return lis

def get_minmax_dates(dash):
    if advisor_available(dash.id) == False:
        return [0,0]
    else:
        with Advisor(dash.id) as adv:
            a = adv.JobStartTime()
            b = adv.JobEndTime()
            c = int(time.mktime(a.timetuple())) * 1000
            d = int(time.mktime(b.timetuple())) * 1000
            return [c,d]

def get_suggestions(dash):
    
    PID = ProblemIdentifier()
    
    suggestions = [{'text' : defaults.TEXT_LIBRARY_CACHE_MISS_RATIO_HIGH,
                'thresholds' : [dash.yellowLibraryCacheMissRatioHighPercent, dash.redLibraryCacheMissRatioHighPercent],
                'pid' : PID.LIBRARY_CACHE_MISS_RATIO_HIGH,
                },
               {'text' : defaults.TEXT_DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH,
                'thresholds' : [dash.yellowDataDictionaryCacheMissRatioHighPercent, dash.redDataDictionaryCacheMissRatioHighPercent],
                'pid' : PID.DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH,
                },
               {'text' : defaults.TEXT_SHARED_POOL_GROSS_USAGE_HIGH,
                'thresholds' : [dash.yellowSharedPoolGrossUsageHighPercent, dash.yellowSharedPoolGrossUsageHighPercent],
                'pid' : PID.SHARED_POOL_GROSS_USAGE_HIGH,
                },
               {'text' : defaults.TEXT_HARD_PARSE_RATIO_HIGH,
                'thresholds' : [dash.yellowHardParseRatioHighPercent, dash.redHardParseRatioHighPercent],
                'pid' : PID.HARD_PARSE_RATIO_HIGH,
                },
               {'text' : defaults.TEXT_BUFFER_CACHE_HIT_RATIO_LOW,
                'thresholds' : [dash.yellowBufferCacheHitRatioLowPercent, dash.redBufferCacheHitRatioLowPercent],
                'pid' : PID.BUFFER_CACHE_HIT_RATIO_LOW,
                },
               {'text' : defaults.TEXT_REDO_LOG_BUFFER_LARGE,
                'thresholds' : [dash.yellowRedoLogBufferLargePercent, dash.redRedoLogBufferLargePercent],
                'pid' : PID.REDO_LOG_BUFFER_LARGE,
                },
               {'text' : defaults.TEXT_REDO_LOG_BUFFER_SMALL,
                'thresholds' : [dash.yellowRedoLogBufferSmallPercent, dash.redRedoLogBufferSmallPercent],
                'pid' : PID.REDO_LOG_BUFFER_SMALL,
                },
               {'text' : defaults.TEXT_SORT_AREA_LARGE,
                'thresholds' : [dash.yellowSortAreaLargePercent, dash.redSortAreaLargePercent],
                'pid' : PID.SORT_AREA_LARGE,
                },
               {'text' : defaults.TEXT_SORT_AREA_SMALL,
                'thresholds' : [dash.yellowSortAreaSmallPercent, dash.redSortAreaSmallPercent],
                'pid' : PID.SORT_AREA_SMALL,
                },
               ]
    
    if advisor_available(dash.id) == False:
#         return dummy values in blue (info)
        for entry in suggestions:
            entry['suggestion'] = None
            entry['color'] = 'info'
            entry['heading'] = entry['text'][0]
            entry['title'] = entry['text'][1]
        return "info", suggestions
    else:
        with Advisor(dash.id) as adv:
            shortInterval = dash.shortIntervalMinutes * 60
            longInterval = dash.longIntervalMinutes * 60
            
            startTime = datetime.strftime(adv.JobEndTime() - timedelta(seconds=shortInterval), adv._datetimeFmt)
            endTime = datetime.strftime(adv.JobEndTime(), adv._datetimeFmt)
    
            for entry in suggestions:
                entry['suggestion'] = {}
                entry['color'] = 'success'
                entry['heading'] = entry['text'][0]
                entry['title'] = entry['text'][1]
                entry['suggestion'] = adv.GetSuggestion(entry['pid'], startTime, endTime, entry['thresholds'][1])
                if entry['suggestion'] is not None:
                    entry['color'] = 'error'
                else:
                    entry['suggestion'] = adv.GetSuggestion(entry['pid'], startTime, endTime, entry['thresholds'][0])
                    if entry['suggestion'] is not None:
                        entry['color'] = 'block'
            top_color = 'success'
            for s in suggestions:
                if s['suggestion'] is not None:
                    if s['color'] == 'error':
                        top_color = 'error'
                    elif s['color'] == 'block' and top_color == 'success':
                        top_color = 'block'
            return top_color, suggestions

def _empty_chartdata(dash):
    SID = StatsIdentifier()
    chartdata =  [{'text' : defaults.TEXT_LIBRARY_CACHE_HIT_RATIO,
                        'thresholds' : [1.0 - x for x in [dash.yellowLibraryCacheMissRatioHighPercent, dash.redLibraryCacheMissRatioHighPercent]],
                        'comparison' : '<',
                        'sid' : SID.LIBRARY_CACHE_HIT_RATIO,
                        'tag' : "chart1",
                        'tooltip' : "hit ratio",
                        },
                 {'text' : defaults.TEXT_ROW_CACHE_HIT_RATIO,
                        'thresholds' : [1.0 - x for x in [dash.yellowDataDictionaryCacheMissRatioHighPercent, dash.redDataDictionaryCacheMissRatioHighPercent]],
                        'comparison' : '<',
                        'sid' : SID.ROW_CACHE_HIT_RATIO,
                        'tag' : "chart2",
                        'tooltip' : "hit ratio",
                        },
                 {'text' : defaults.TEXT_SHARED_POOL_GROSS_USAGE_PCT,
                        'thresholds' : [dash.yellowSharedPoolGrossUsageHighPercent, dash.yellowSharedPoolGrossUsageHighPercent],
                        'comparison' : '>',
                        'sid' : SID.SHARED_POOL_GROSS_USAGE_PCT,
                        'tag' : "chart3",
                        'tooltip' : "usage",
                        },
                 {'text' : defaults.TEXT_HARD_PARSE_RATIO,
                        'thresholds' : [dash.yellowHardParseRatioHighPercent, dash.redHardParseRatioHighPercent],
                        'comparison' : '>',
                        'sid' : SID.HARD_PARSE_RATIO,
                        'tag' : "chart4",
                        'tooltip' : "hard parse ratio",
                        },
                 {'text' : defaults.TEXT_BUFFER_CACHE_HIT_RATIO,
                        'thresholds' :  [dash.yellowBufferCacheHitRatioLowPercent, dash.redBufferCacheHitRatioLowPercent],
                        'comparison' : '<',
                        'sid' : SID.BUFFER_CACHE_HIT_RATIO,
                        'tag' : "chart5",
                        'tooltip' : "hit ratio",
                        },
                 {'text' : defaults.TEXT_REDO_BUFFER_HIT_RATIO,
                        'thresholds' : [dash.yellowRedoLogBufferLargePercent, dash.redRedoLogBufferLargePercent],
                        'comparison' : '>',
                        'thresholds2' : [dash.yellowRedoLogBufferSmallPercent, dash.redRedoLogBufferSmallPercent],
                        'comparison2' : '<',
                        'sid' : SID.REDO_BUFFER_HIT_RATIO,
                        'tag' : "chart6",
                        'tooltip' : "hit ratio",
                        },
                 {'text' : defaults.TEXT_SORTS_DISK_RATIO,
                        'thresholds' : [dash.yellowSortAreaLargePercent, dash.redSortAreaLargePercent],
                        'comparison' : '>',
                        'thresholds2' : [dash.yellowSortAreaSmallPercent, dash.redSortAreaSmallPercent],
                        'comparison2' : '<',
                        'sid' : SID.SORTS_DISK_RATIO,
                        'tag' : "chart7",
                        'tooltip' : "sorts disk ratio",
                        }
                       ]
    for chart in chartdata:
        chart['data'] = []
        chart['heading'] = chart['text'][0]
        chart['title'] = chart['text'][1]
        chart['subtitle'] = 'This chart is empty, as no data is available'
    return chartdata

def get_latest_charts(dash, shortInterval, longInterval):
    if advisor_available(dash.id) == False:
        return _get_empty_charts(dash, shortInterval, longInterval)
    else:
        with Advisor(dash.id) as adv:
            startTime = datetime.strftime(adv.JobStartTime(), adv._datetimeFmt)
            endTime = datetime.strftime(adv.JobEndTime(), adv._datetimeFmt)
            return _get_filled_charts(dash, startTime, endTime, shortInterval, longInterval)
    
def get_charts(dash, start, end, shortInterval, longInterval):
    if advisor_available(dash.id) == False:
        return _get_empty_charts(dash, shortInterval, longInterval)
    else:
        with Advisor(dash.id) as adv:
            startTime = datetime.strftime(start, adv._datetimeFmt)
            endTime = datetime.strftime(end, adv._datetimeFmt)
            return _get_filled_charts(dash, startTime, endTime, shortInterval, longInterval)

def _get_empty_charts(dash, short, long):
    chartdata = _empty_chartdata(dash)
    for chart in chartdata:
        chart['data'] = []
        return chartdata

def _get_filled_charts(dash, startTime, endTime, short, long):
    chartdata = _empty_chartdata(dash)
    with Advisor(dash.id) as adv:
        coarseInterval = long * 60
        fineInterval = short * 60
        fineIntervalDelta = timedelta(seconds=fineInterval)
        
        for chart in chartdata:
            chart['subtitle'] = 'Date range: '+startTime+' to '+endTime
            
            coarseTuples = adv.GetStats(chart['sid'], startTime, endTime, coarseInterval)
            data = []
            for tuple in coarseTuples:
                value = tuple[0]
                drilldown = []
                drillStart = tuple[1]
                drillEnd = datetime.strftime(datetime.strptime(tuple[1], adv._datetimeFmt) + fineIntervalDelta*(long/short), adv._datetimeFmt)
                drilldownTuples = adv.GetStats(chart['sid'], drillStart, drillEnd, fineInterval)
                for t in drilldownTuples:
                    v = t[0]
                    drilldown.append({
                                      'value' : round(v*100,1), 
                                      'color' : _get_color(v,chart),
                                      'category' : t[2]})
                data.append({
                             'value' : round(value*100,1), 
                             'color' : _get_color(value,chart),
                             'category' : tuple[2], 
                             'drilldown' : drilldown})
            chart['data'] = data   
        return chartdata
    
def _get_color(value, chart):
    threshold = chart['thresholds']
    comparison = chart['comparison']
    color = 'green'
    yellow = threshold[0]
    red = threshold[1]
    if comparison == '<':
        if value < red:
            color = 'red'
        elif value < yellow:
            color = 'yellow'
    else:
        if value > red:
            color = 'red'
        elif value > yellow:
            color = 'yellow'
    if color != 'red' and 'thresholds2' in chart:   
        threshold = chart['thresholds2']
        comparison = chart['comparison2']
        if comparison == '<':
            if value < red:
                color = 'red'
            elif value < yellow:
                color = 'yellow'
        else:
            if value > red:
                color = 'red'
            elif value > yellow:
                color = 'yellow'
    return color



