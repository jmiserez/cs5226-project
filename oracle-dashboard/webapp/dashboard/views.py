from django.http import HttpResponse, HttpResponseRedirect, HttpRequest, Http404
from django.shortcuts import render, render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from models import *
from forms import *
from dashboard import external
from dashboard import stats
from datetime import datetime, timedelta
import json
import stats
import urllib
from backend.Advisor import Advisor
from django.views.decorators.cache import never_cache

def get_existing_dashboard_or_404(dashboard_id):
    dash = get_object_or_404(Dashboard, pk=dashboard_id)
    if dash.deleted:
        raise Http404('Dashboard %s was deleted.' % dash.id)
    return dash

def redirect_to_dashboard(request):
    return HttpResponseRedirect("/dashboard/")

def redirect_to_view_dashboard(request, dashboard_id):
    return HttpResponseRedirect('/dashboard/'+str(dashboard_id))

def home(request):
    dashboards = Dashboard.objects.all()
    context = {'dashboards' : dashboards}
    return render(request, 'home.html', context)

def overview(request):
    return render(request, 'overview_graph.html')

def about(request):
    dashboards = Dashboard.objects.all()
    context = {'dashboards' : dashboards}
    return render(request, 'about.html', context)

def add_dashboard(request):
    dashboards = Dashboard.objects.all()
    if request.method == 'POST': 
        form = AddDashboardForm(request.POST)
        if form.is_valid(): #checks csrf and contents
            dash = Dashboard(
                name = form.cleaned_data['name'],
                host = form.cleaned_data['host'],
                port = form.cleaned_data['port'],
                sid = form.cleaned_data['sid'],
                username = form.cleaned_data['username'],
                password = form.cleaned_data['password']
                )
            dash.save()
            return HttpResponseRedirect('/dashboard/'+str(dash.id)) # Redirect after POST
    else:
        form = AddDashboardForm() # An unbound form

    context = {'dashboards' : dashboards, 'form': form,}
    return render(request, 'add_dashboard.html', context)

def jsquote(s):
    return "'" + s.replace("'", "'+\"'\"+'") + "'"

@never_cache
def view_dashboard(request, dashboard_id):
    if request.method == 'GET':
        dashboards = Dashboard.objects.all()
        rawSqlhistory = SqlHistory.objects.filter(dashboard = dashboard_id)
        sqlhistory=[]
        for entry in rawSqlhistory:
            safecmd = entry.cmd
            safecmd = safecmd.replace("\n", "\\n")
            safecmd = jsquote(safecmd)
            sqlhistory.insert(0,{"safecmd" : safecmd, "cmd" : entry.cmd, "answer" : entry.answer})
        dash = get_existing_dashboard_or_404(dashboard_id)
        topform = ChangeStateForm() # An unbound form
        settingsform = SettingsForm({
                                     'shortIntervalMinutes': dash.shortIntervalMinutes,
                                     'longIntervalMinutes': dash.longIntervalMinutes,
                                     'yellowLibraryCacheMissRatioHighPercent' : 100 * dash.yellowLibraryCacheMissRatioHighPercent,
                                     'yellowDataDictionaryCacheMissRatioHighPercent' : 100 * dash.yellowDataDictionaryCacheMissRatioHighPercent,
                                     'yellowSharedPoolGrossUsageHighPercent' : 100 * dash.yellowSharedPoolGrossUsageHighPercent,
                                     'yellowHardParseRatioHighPercent' : 100 * dash.yellowHardParseRatioHighPercent,
                                     'yellowBufferCacheHitRatioLowPercent' : 100 * dash.yellowBufferCacheHitRatioLowPercent,
                                     'yellowRedoLogBufferSmallPercent' : 100 * dash.yellowRedoLogBufferSmallPercent,
                                     'yellowRedoLogBufferLargePercent' : 100 * dash.yellowRedoLogBufferLargePercent,
                                     'yellowSortAreaSmallPercent' : 100 * dash.yellowSortAreaSmallPercent,
                                     'yellowSortAreaLargePercent' : 100 * dash.yellowSortAreaLargePercent,
                                     'yellowSqlElapsedSeconds' : dash.yellowSqlElapsedSeconds,
                                     'redLibraryCacheMissRatioHighPercent' : 100 * dash.redLibraryCacheMissRatioHighPercent,
                                     'redDataDictionaryCacheMissRatioHighPercent' : 100 * dash.redDataDictionaryCacheMissRatioHighPercent,
                                     'redSharedPoolGrossUsageHighPercent' : 100 * dash.redSharedPoolGrossUsageHighPercent,
                                     'redHardParseRatioHighPercent' : 100 * dash.redHardParseRatioHighPercent,
                                     'redBufferCacheHitRatioLowPercent' : 100 * dash.redBufferCacheHitRatioLowPercent,
                                     'redRedoLogBufferSmallPercent' : 100 * dash.redRedoLogBufferSmallPercent,
                                     'redRedoLogBufferLargePercent' : 100 * dash.redRedoLogBufferLargePercent,
                                     'redSortAreaSmallPercent' : 100 * dash.redSortAreaSmallPercent,
                                     'redSortAreaLargePercent' : 100 * dash.redSortAreaLargePercent,
                                    'redSqlElapsedSeconds' : dash.redSqlElapsedSeconds,
                                     })
        top_color, suggestions = stats.get_suggestions(dash)
        charts = stats.get_latest_charts(dash, dash.shortIntervalMinutes, dash.longIntervalMinutes)
        low_sql = stats.get_low_sql(dashboard_id)
        long_sql = stats.get_long_sql(dashboard_id)
        cpu_intensive = stats.cpu_intensive_session(dashboard_id)
        minmax_dates = stats.get_minmax_dates(dash)
        ondemandform = OnDemandForm(initial={
                                     'short': dash.shortIntervalMinutes,
                                     'long': dash.longIntervalMinutes
                                     })
        if 'short' in request.GET and 'long' in request.GET and 'start' in request.GET and 'end' in request.GET:
            short = request.GET['short']
            long = request.GET['long']
            start = datetime.strptime(request.GET['start'], '%Y-%m-%d %H:%M:%S')
            end = datetime.strptime(request.GET['end'], '%Y-%m-%d %H:%M:%S')
            ondemandform = OnDemandForm({
                                      'short': short,
                                      'long': long,
                                      'start' : start,
                                      'end' : end
                                      })
            if ondemandform.is_valid():
                charts = stats.get_charts(dash, ondemandform.cleaned_data['start'], ondemandform.cleaned_data['end'], ondemandform.cleaned_data['short'], ondemandform.cleaned_data['long'])
            else:
                charts = stats.get_latest_charts(dash, dash.shortIntervalMinutes, dash.longIntervalMinutes)
        elif 'short' in request.GET and 'long' in request.GET:
            short = request.GET['short']
            long = request.GET['long']
            shortint = int(short)
            longint = int(long)
            ondemandform = OnDemandForm(initial={
                             'short': shortint,
                             'long': longint
                             })
            charts = stats.get_latest_charts(dash, shortint, longint)
        else:
            ondemandform = OnDemandForm(initial={
                                     'short': dash.shortIntervalMinutes,
                                     'long': dash.longIntervalMinutes
                                     })
            charts = stats.get_latest_charts(dash, dash.shortIntervalMinutes, dash.longIntervalMinutes)
        context = {'dashboards' : dashboards, 
               'dash' : dash,
               'sqlhistory' : sqlhistory, 
               'topform' : topform, 
               'settingsform' : settingsform, 
               'ondemandform' : ondemandform, 
               'suggestions' : suggestions, 
               'top_color' : top_color,
               'minDate' : minmax_dates[0],
               'maxDate' : minmax_dates[1],
               'charts' : charts,
               'low_sql': low_sql,
               'long_sql': long_sql,
               'cpu_intensive': cpu_intensive}
        return render(request, 'view_dashboard.html', context)
        

def save_settings(request, dashboard_id):
    if request.method == 'POST':
        dash = get_existing_dashboard_or_404(dashboard_id)
        settingsform = SettingsForm(request.POST)
        if settingsform.is_valid(): #checks csrf and contents
            dash.shortIntervalMinutes = settingsform.cleaned_data['shortIntervalMinutes']
            dash.longIntervalMinutes = settingsform.cleaned_data['longIntervalMinutes']
            dash.yellowLibraryCacheMissRatioHighPercent = settingsform.cleaned_data['yellowLibraryCacheMissRatioHighPercent'] / 100.0
            dash.yellowDataDictionaryCacheMissRatioHighPercent = settingsform.cleaned_data['yellowDataDictionaryCacheMissRatioHighPercent'] / 100.0
            dash.yellowSharedPoolGrossUsageHighPercent = settingsform.cleaned_data['yellowSharedPoolGrossUsageHighPercent'] / 100.0
            dash.yellowHardParseRatioHighPercent = settingsform.cleaned_data['yellowHardParseRatioHighPercent'] / 100.0
            dash.yellowBufferCacheHitRatioLowPercent = settingsform.cleaned_data['yellowBufferCacheHitRatioLowPercent'] / 100.0
            dash.yellowRedoLogBufferSmallPercent = settingsform.cleaned_data['yellowRedoLogBufferSmallPercent'] / 100.0
            dash.yellowRedoLogBufferLargePercent = settingsform.cleaned_data['yellowRedoLogBufferLargePercent'] / 100.0
            dash.yellowSortAreaSmallPercent = settingsform.cleaned_data['yellowSortAreaSmallPercent'] / 100.0
            dash.yellowSortAreaLargePercent = settingsform.cleaned_data['yellowSortAreaLargePercent'] / 100.0
            dash.yellowSqlElapsedSeconds = settingsform.cleaned_data['yellowSqlElapsedSeconds']
            dash.redLibraryCacheMissRatioHighPercent = settingsform.cleaned_data['redLibraryCacheMissRatioHighPercent'] / 100.0
            dash.redDataDictionaryCacheMissRatioHighPercent = settingsform.cleaned_data['redDataDictionaryCacheMissRatioHighPercent'] / 100.0
            dash.redSharedPoolGrossUsageHighPercent = settingsform.cleaned_data['redSharedPoolGrossUsageHighPercent'] / 100.0
            dash.redHardParseRatioHighPercent = settingsform.cleaned_data['redHardParseRatioHighPercent'] / 100.0
            dash.redBufferCacheHitRatioLowPercent = settingsform.cleaned_data['redBufferCacheHitRatioLowPercent'] / 100.0
            dash.redRedoLogBufferSmallPercent = settingsform.cleaned_data['redRedoLogBufferSmallPercent'] / 100.0
            dash.redRedoLogBufferLargePercent = settingsform.cleaned_data['redRedoLogBufferLargePercent'] / 100.0
            dash.redSortAreaSmallPercent = settingsform.cleaned_data['redSortAreaSmallPercent'] / 100.0
            dash.redSortAreaLargePercent = settingsform.cleaned_data['redSortAreaLargePercent'] / 100.0
            dash.redSqlElapsedSeconds = settingsform.cleaned_data['redSqlElapsedSeconds']
            dash.save()
            return HttpResponseRedirect('/dashboard/'+str(dash.id)+'#tab_settings') # Redirect after POST
    else:
        return HttpResponseRedirect('/dashboard/'+str(dash.id))

def generate_ondemand(request, dashboard_id):
    if request.method == 'POST':
        dash = get_existing_dashboard_or_404(dashboard_id)
        ondemandform = OnDemandForm(request.POST)
        ondemandform.is_valid() #process
        args = {}
        if 'short' in ondemandform.cleaned_data:
            args['short'] = ondemandform.cleaned_data['short']
        if 'long' in ondemandform.cleaned_data:
            args['long'] = ondemandform.cleaned_data['long']
        if 'start' in ondemandform.cleaned_data:
            args['start'] = datetime.strftime(ondemandform.cleaned_data['start'], '%Y-%m-%d %H:%M:%S')
        if 'end' in ondemandform.cleaned_data:
            args['end'] = datetime.strftime(ondemandform.cleaned_data['end'], '%Y-%m-%d %H:%M:%S')
        return HttpResponseRedirect('/dashboard/'+str(dash.id)+'?'+urllib.urlencode(args) + "#tab_reports") # Redirect after POST

def change_state(request, dashboard_id):
    dash = get_existing_dashboard_or_404(dashboard_id)
    context = {'dash' : dash}
    if request.method == 'POST': 
        topform = ChangeStateForm(request.POST)
        if topform.is_valid(): #checks csrf and contents
            if request.POST['changestateaction'] == 'start':
                external.startJob(dash.id, dash.username, dash.password, dash.makeDsn())
                dash.active = True
                dash.save()
            elif request.POST['changestateaction'] == 'stop':
                external.stopJob(dash.id)
                dash.active = False
                dash.save()
            elif request.POST['changestateaction'] == 'restart':
                external.stopJob(dash.id)
                dash.active = False
                dash.save()
                external.startJob(dash.id, dash.username, dash.password, dash.makeDsn())
                dash.active = True
                dash.save()
            elif request.POST['changestateaction'] == 'sync':
                jobids = external.listJobs()
                dash.active = False
                for i in jobids:
                    if dash.id == i:
                        dash.active = True
                dash.save()
            elif request.POST['changestateaction'] == 'delete':
                external.stopJob(dash.id)
                dash.deleted = True
                dash.active = False
                dash.save()
                return HttpResponseRedirect('/dashboard/')
        return HttpResponseRedirect('/dashboard/'+str(dash.id)) # Redirect after POST

@csrf_exempt #makes ajax easier with POST
def check_connection(request):
    if request.method == 'POST':
        tmp = Dashboard(name = request.POST['name'],
                        host = request.POST['host'],
                        port = request.POST['port'],
                        sid = request.POST['sid'],
                        username = request.POST['username'],
                        password = request.POST['password'])
        tns = tmp.tnsSqlPlusString()
        if external.checkConnection(tns):
            return HttpResponse(content="Ok")
        else:
            return HttpResponse(content="Fail")
        
def check_connection2(request, dashboard_id):
    dash = get_existing_dashboard_or_404(dashboard_id)
    if external.checkConnection(dash.tnsSqlPlusString()):
        return HttpResponse(content="Ok")
    else:
        return HttpResponse(content="Fail")

@csrf_exempt #makes ajax easier with POST
def execute_remote(request, dashboard_id):
    if request.method == 'POST':
        dash = get_existing_dashboard_or_404(dashboard_id)
        newcmd = request.body
        newanswer = external.executeRemote(dash.tnsSqlPlusString(), newcmd)
        entry = SqlHistory(dashboard = dash,
                           cmd = newcmd,
                           answer = newanswer)
        entry.save()
        text = 'SQL> ' + newcmd + "\n" + newanswer
        return HttpResponse(content=text)



###### GET CHART DATA FUNCTIONS ########
@never_cache
def get_stats(request):
    job_id = int(request.GET['job_id'])
    stat_id = int(request.GET['stat_id'])
    interval = int(request.GET['interval'])
    t_end = datetime.now()
    t_start = (t_end - timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
    t_end = t_end.strftime("%Y-%m-%d %H:%M:%S")

    with Advisor(job_id) as adr:
        lis = adr.GetStats(stat_id, t_start, t_end, interval)

    _json_data = []

    for data in lis:
        _json_data.append({'time':  data[2], 'value': data[0]})#int(time.mktime(data_time.timetuple()))
    return HttpResponse(json.dumps(_json_data), mimetype="application/json")

def get_latest_stats(request):
    job_id = int(request.GET['job_id'])
    stat_id = int(request.GET['stat_id'])
    interval = int(request.GET['interval'])
    t_end = datetime.now()
    t_start = (t_end - timedelta(seconds=90)).strftime("%Y-%m-%d %H:%M:%S")

    t_end = t_end.strftime("%Y-%m-%d %H:%M:%S")
    with Advisor(job_id) as adr:
        lis = adr.GetStats(stat_id, t_start, t_end, interval)

    _json_data = []

    for data in lis:
        _json_data.append({'time': data[2], 'value': data[0]})
    return HttpResponse(json.dumps(_json_data), mimetype="application/json")
