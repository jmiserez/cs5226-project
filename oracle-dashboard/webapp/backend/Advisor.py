#!/usr/bin/env/python2
# coding=utf-8
__author__ = 'xinyu'

import sqlite3

from datetime import datetime, timedelta
from traceback import print_tb

from Config import STATS_DB_FILENAME, SQLITE_DATETIME_FORMAT, DIRECT_CALL_PATH_PREFIX, SQL_TUNING_RETURN_LIMIT

# CONSTANTS

# SQLs
GET_JOB_START_DATETIME_SQL = 'SELECT START_TIME FROM JOB_REGISTRY WHERE JOB_ID = ?'
GET_JOB_END_DATETIME_SQL = 'SELECT END_TIME FROM JOB_REGISTRY WHERE JOB_ID = ?'
CHECK_JOB_EXISTS_SQL = 'SELECT JOB_ID FROM JOB_REGISTRY WHERE JOB_ID = ?'

GET_FIRST_ROW_FOR_BLOCK_TEMPLATE = '''
SELECT {columns} FROM {table} WHERE JOB_ID = ? AND UPDATE_TIME >= ?  AND UPDATE_TIME <= ? ORDER BY UPDATE_TIME LIMIT 1
'''
GET_LAST_ROW_FOR_BLOCK_TEMPLATE = '''
SELECT {columns} FROM {table} WHERE JOB_ID = ? AND UPDATE_TIME <= ? AND UPDATE_TIME >= ?
ORDER BY UPDATE_TIME DESC LIMIT 1
'''

GET_DAT_BETWEEN_TIME_RANGE_TEMPLATE = '''
SELECT {columns} FROM {table} WHERE JOB_ID = ? AND UPDATE_TIME BETWEEN ? AND ? ORDER BY UPDATE_TIME
'''

GET_SUGGESTION_SQL = 'SELECT PROBLEM_DESCRIPTION, SOLUTION, REFERENCE, INIT_PARAMETER, QUERY ' \
                     'FROM SUGGESTIONS WHERE PROBLEM_ID = ?'

GET_LONG_RUN_SQL = 'SELECT * FROM LONG_RUN_SQL WHERE JOB_ID = ? AND DATE(UPDATE_TIME) = DATE(?) ' \
                   'ORDER BY SOFAR + ELAPSED_SECONDS DESC LIMIT {}'.format(SQL_TUNING_RETURN_LIMIT)

GET_LOW_EFFICIENCY_SQL = 'SELECT * FROM LOW_EFFICIENCY_SQL WHERE JOB_ID = ? AND DATE(UPDATE_TIME) = DATE(?) ' \
                         'ORDER BY BUFFER_PER_EXEC DESC LIMIT {}'.format(SQL_TUNING_RETURN_LIMIT)

GET_CPU_INTENSIVE_SQL = 'SELECT * FROM CPU_INTENSIVE_SESSION WHERE JOB_ID = ? ' \
                        'AND DATE(UPDATE_TIME) = DATE(?) ORDER BY VALUE DESC LIMIT {}'.format(SQL_TUNING_RETURN_LIMIT)


# Identifier for all the stats interested
class StatsIdentifier(object):
    def __init__(self):
        # buffer cache
        self.BUFFER_CACHE_HIT_RATIO = 1
        # shared library
        self.LIBRARY_CACHE_HIT_RATIO = 2
        self.ROW_CACHE_HIT_RATIO = 3
        self.SHARED_POOL_GROSS_USAGE_PCT = 4
        self.HARD_PARSE_RATIO = 5
        # redo buffer size
        self.REDO_BUFFER_HIT_RATIO = 6
        # sorts memory
        self.SORTS_DISK_RATIO = 7


# Identifier for all the problems
class ProblemIdentifier(object):
    def __init__(self):
        # shared pool
        self.LIBRARY_CACHE_MISS_RATIO_HIGH = 1
        self.DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH = 2
        self.SHARED_POOL_GROSS_USAGE_HIGH = 3
        self.HARD_PARSE_RATIO_HIGH = 4
        # buffer cache
        self.BUFFER_CACHE_HIT_RATIO_LOW = 5
        # redo log
        self.REDO_LOG_BUFFER_SMALL = 6
        self.REDO_LOG_BUFFER_LARGE = 7
        # sort memory
        self.SORT_AREA_SMALL = 8
        self.SORT_AREA_LARGE = 9


class Advisor(object):
    """
    Advisor serves as an interface layer:
    1) provides functions to retrieve data from stats.db to render graphs
    2) retrieve solutions formatted with threshold supplied to display
    """

    def __init__(self, jobID,  statsDB=DIRECT_CALL_PATH_PREFIX + STATS_DB_FILENAME,
                 datetimeFmt=SQLITE_DATETIME_FORMAT):
        # caller is responsible for passing an valid jobID
        self._jobID = jobID
        self._jobStartTime = None

        self._statsDB = statsDB
        self._conn = None

        self._datetimeFmt = datetimeFmt

        # helper class
        self._statsID = StatsIdentifier()
        self._problemID = ProblemIdentifier()

        self._statsRetrievers = self._initStatsRetrievers()
        self._suggestionMaker = self._initSuggestionMaker()

    def openConnection(self):
        self._conn = sqlite3.connect(self._statsDB)
        self._conn.row_factory = sqlite3.Row

    def closeConnection(self):
        self._conn.close()

    def __enter__(self):
        try:
            self.openConnection()
            self._checkJobExists()
            self._jobStartTime = self._getJobStartTime()
        except:
            print "Open connection to database {} failed ".format(self._statsDB)
            raise
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # close on a closed connection would not raise exception
        self.closeConnection()
        if exc_type is not None:
            print_tb(exc_tb)
            raise exc_type(exc_val)

    def _initStatsRetrievers(self):
        # map each stats id to the corresponding retrieve function
        retriever = {
            self._statsID.BUFFER_CACHE_HIT_RATIO: self._getBufferCacheHitRatio,
            self._statsID.LIBRARY_CACHE_HIT_RATIO: self._getLibraryCacheHitRatio,
            self._statsID.ROW_CACHE_HIT_RATIO: self._getRowCacheHitRatio,
            self._statsID.SHARED_POOL_GROSS_USAGE_PCT: self._getSharedPoolGrossUsagePct,
            self._statsID.HARD_PARSE_RATIO: self._getHardParseRatio,
            self._statsID.REDO_BUFFER_HIT_RATIO: self._getRedoBufferHitRatio,
            self._statsID.SORTS_DISK_RATIO: self._getSortsDiskRatio
        }

        return retriever

    def _initSuggestionMaker(self):
        # map each problem id to the corresponding suggestion function
        suggestionMaker = {
            self._problemID.BUFFER_CACHE_HIT_RATIO_LOW: self._adviseBufferCacheHitRatioLow,
            self._problemID.LIBRARY_CACHE_MISS_RATIO_HIGH: self._adviseLibraryCacheMissRatioHigh,
            self._problemID.DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH: self._adviseDataDictCacheMissRatioHigh,
            self._problemID.SHARED_POOL_GROSS_USAGE_HIGH: self._adviseSharedPoolGrossUsageHigh,
            self._problemID.HARD_PARSE_RATIO_HIGH: self._adviseHardParseRatioHigh,
            self._problemID.REDO_LOG_BUFFER_SMALL: self._adviseRedoLogBufferSmall,
            self._problemID.REDO_LOG_BUFFER_LARGE: self._adviseRedoLogBufferLarge,
            self._problemID.SORT_AREA_SMALL: self._adviseSortAreaSmall,
            self._problemID.SORT_AREA_LARGE: self._adviseSortAreaLarge
        }

        return suggestionMaker

    def _getJobStartTime(self):
        # get the start time by job_id
        # () -> datetime.datetime
        startTime = self._conn.execute(GET_JOB_START_DATETIME_SQL, (self._jobID, )).fetchone()['START_TIME']
        return datetime.strptime(startTime, self._datetimeFmt)

    def _checkJobExists(self):
        # check whether job has been registered in database
        cur = self._conn.execute(CHECK_JOB_EXISTS_SQL, (self._jobID, )).fetchone()
        if cur is None:
            raise ValueError("No JOB ID {} exists in database".format(self._jobID))

    def JobStartTime(self):
        # get the start time of the job
        # () -> datetime.datetime
        if self._jobStartTime is None:
            self._getJobStartTime()
        return self._jobStartTime

    def JobEndTime(self):
        # get the end time of the job
        # job could be still running
        # () -> datetime.datetime
        cur = self._conn.execute(GET_JOB_END_DATETIME_SQL, (self._jobID, ))
        if cur is None:
            # job still running
            # properly terminated job won't leave a job entry with end_time being null
            return datetime.now()
        else:
            try:
                return datetime.strptime(cur.fetchone()['END_TIME'], self._datetimeFmt)
            except:
                return datetime.now()

    @property
    def JobID(self):
        return self._jobID

    # FOR frontend
    def GetStats(self, statsID, startTime, endTime, interval):
        # always return a tuple, the num of elements are determined by interval
        # the job is assumed to being running continuously between startTime and endTime
        # thus it is the caller's responsible to pass valid startTime and endTime
        # stats used will be strictly within the range (startTime, endTime) not []
        # startTime and endTime are assumed to be strings formatted as self._datetimeFmt

        rtvFunc = self._statsRetrievers[statsID]  # function to do the retrieve
        _interval = timedelta(seconds=interval)
        _startTime = datetime.strptime(startTime, self._datetimeFmt)
        _endTime = datetime.strptime(endTime, self._datetimeFmt)

        stats = []
        while _endTime > _startTime + _interval:
            # append data for one block
            stat = rtvFunc(_startTime, _startTime + _interval)
            if stat is not None:
                stats.append(stat)
            _startTime = _startTime + _interval

        return tuple(stats)

    def GetSuggestion(self, problemID, startTime, endTime, threshold):
        # problemID is defined in the class ProblemIdentifier
        # startTime and endTime are assumed to be strings formatted as self._datetimeFmt
        #   and are valid within the range (JobStartTime, JobEndTime)
        # if no suggestion to make, return None
        sugFunc = self._suggestionMaker[problemID]  
        _startTime = datetime.strptime(startTime, self._datetimeFmt)
        _endTime = datetime.strptime(endTime, self._datetimeFmt)
        return sugFunc(_startTime, _endTime, threshold)

    def GetLongRunSql(self, dtime):
        values = (self._jobID, datetime.strftime(dtime, self._datetimeFmt))
        rows = self._conn.execute(GET_LONG_RUN_SQL, values).fetchall()
        res = []
        for row in rows:
            res.append({k: row[k] for k in row.keys()})
        return res

    def GetCPUIntensiveSession(self, dtime):
        values = (self._jobID, datetime.strftime(dtime, self._datetimeFmt))
        rows = self._conn.execute(GET_CPU_INTENSIVE_SQL, values).fetchall()
        res = []
        for row in rows:
            res.append({k: row[k] for k in row.keys()})
        return res

    def GetLowEfficiencySql(self, dtime):
        values = (self._jobID, datetime.strftime(dtime, self._datetimeFmt))
        rows = self._conn.execute(GET_LOW_EFFICIENCY_SQL, values).fetchall()
        res = []
        for row in rows:
            res.append({k: row[k] for k in row.keys()})
        return res

    # individual retrieve functions
    # all in the form of func(startTime, endTime) -> (firstDatTime, lastDatTime, stat)
    # startTime and endTime are datetime.datetime
    def _getBufferCacheHitRatio(self, startTime, endTime):

        table = 'BUFFER_POOL_HIT_RATIO'
        columns = ['UPDATE_TIME', 'DB_BLOCK_GETS_FROM_CACHE',
                   'CONSISTENT_GETS_FROM_CACHE', 'PHYSICAL_READS_CACHE']

        firstDat, lastDat = self._retrieveBlockEndPointDat(table, columns, startTime, endTime)
        if firstDat is not None and lastDat is not None:
            blockTimeRange, blockGetsBlk, consistentGetsBlk, physicalBreadsBlk = zip(firstDat, lastDat)
        else:
            return

        dif = lambda dat: dat[-1] - dat[0]
        totalGets = dif(blockGetsBlk) + dif(consistentGetsBlk)
        if totalGets == 0:
            hitRatio = 1.0
        else:
            hitGets = totalGets - dif(physicalBreadsBlk)
            hitRatio = hitGets / float(totalGets)

        return hitRatio, blockTimeRange[0], blockTimeRange[1]

    def _getLibraryCacheHitRatio(self, startTime, endTime):

        table = 'LIBRARY_CACHE'
        columns = ['UPDATE_TIME', 'PINS_SUM', 'PINHITS_SUM']

        firstDat, lastDat = self._retrieveBlockEndPointDat(table, columns, startTime, endTime)
        if firstDat is not None and lastDat is not None:
            blockTimeRange, pinsSum, pinhitsSum = zip(firstDat, lastDat)
        else:
            return
        dif = lambda dat: dat[-1] - dat[0]
        totalPins = dif(pinsSum)
        if totalPins == 0:
            hitRatio = 1.0
        else:
            hitsPin = dif(pinhitsSum)
            hitRatio = hitsPin / float(totalPins)

        return hitRatio, blockTimeRange[0], blockTimeRange[1]

    def _getRowCacheHitRatio(self, startTime, endTime):

        table = 'ROW_CACHE'
        columns = ['UPDATE_TIME', 'FIXED_SUM', 'GETS_SUM', 'GETMISSES_SUM']

        firstDat, lastDat = self._retrieveBlockEndPointDat(table, columns, startTime, endTime)
        if firstDat is not None and lastDat is not None:
            blockTimeRange, fixedSum, getsSum, getmissesSum = zip(firstDat, lastDat)
        else:
            return
        dif = lambda dat: dat[-1] - dat[0]
        totalGets = dif(getsSum)
        if totalGets == 0:
            hitRatio = 1.0
        else:
            hitsGets = totalGets - dif(getmissesSum) - dif(fixedSum)
            hitRatio = hitsGets / float(totalGets)

        return hitRatio, blockTimeRange[0], blockTimeRange[1]

    def _getSharedPoolGrossUsagePct(self, startTime, endTime):

        table = 'SHARED_POOL_GROSS_USAGE'
        columns = ['UPDATE_TIME', 'SHARED_POOL_USED, SHARED_POOL_SIZE']

        dat = self._retrieveBlockAllDat(table, columns, startTime, endTime)
        if dat is not None:
            blockTimeRange = []
            grossUsage = []
            sharedPoolSize = []
            for utime, usage, siz in dat:
                blockTimeRange.append(utime)
                grossUsage.append(usage)
                sharedPoolSize.append(siz)

            if sum(sharedPoolSize) == 0 or len(grossUsage) == 0:
                return
            grossUsagePct = sum(grossUsage) / float(sum(sharedPoolSize)) / len(grossUsage)
            return grossUsagePct, blockTimeRange[0], blockTimeRange[-1]
        else:
            return

    def _getHardParseRatio(self, startTime, endTime):

        table = 'HARD_SOFT_PARSE'
        columns = ['UPDATE_TIME', 'PARSE_COUNT_TOTAL', 'PARSE_COUNT_HARD']

        firstDat, lastDat = self._retrieveBlockEndPointDat(table, columns, startTime, endTime)
        if firstDat is not None and lastDat is not None:
            blockTimeRange, parseTotal, parseHard = zip(firstDat, lastDat)
        else:
            return
        dif = lambda dat: dat[-1] - dat[0]
        totalParse = dif(parseTotal)
        if totalParse == 0:
            parseRatio = 0.0
        else:
            parseRatio = dif(parseHard) / float(totalParse)

        return parseRatio, blockTimeRange[0], blockTimeRange[1]

    def _getRedoBufferHitRatio(self, startTime, endTime):
        table = 'REDO_LOG_STAT'
        columns = ['UPDATE_TIME', 'REDO_ENTRIES', 'REDO_BUFFER_ALLOCATION_RETRIES']

        firstDat, lastDat = self._retrieveBlockEndPointDat(table, columns, startTime, endTime)
        if firstDat is not None and lastDat is not None:
            blockTimeRange, redoEntries, redoAlloRetries = zip(firstDat, lastDat)
        else:
            return
        dif = lambda dat: dat[-1] - dat[0]
        totalEntries = dif(redoEntries)
        if totalEntries == 0:
            retryRatio = 0.0
        else:
            retryRatio = dif(redoAlloRetries) / float(totalEntries)

        return 1 - retryRatio, blockTimeRange[0], blockTimeRange[1]

    def _getSortsDiskRatio(self, startTime, endTime):
        table = 'SORTS_MEM_STAT'
        columns = ['UPDATE_TIME', 'SORTS_MEMORY', 'SORTS_DISK']

        firstDat, lastDat = self._retrieveBlockEndPointDat(table, columns, startTime, endTime)
        if firstDat is not None and lastDat is not None:
            blockTimeRange, sortsMem, sortsDsk = zip(firstDat, lastDat)
        else:
            return
        dif = lambda dat: dat[-1] - dat[0]
        totalSorts = dif(sortsDsk) + dif(sortsMem)
        if totalSorts == 0:
            sortsDskRatio = 0.0
        else:
            sortsDskRatio = dif(sortsDsk) / float(totalSorts)

        return sortsDskRatio, blockTimeRange[0], blockTimeRange[1]

    # individual functions to make suggestions
    def _adviseBufferCacheHitRatioLow(self, startTime, endTime, threshold):
        stats = self._getBufferCacheHitRatio(startTime, endTime)
        if stats is not None and stats[0] < threshold:
            return self._retrieveSuggestion(self._problemID.BUFFER_CACHE_HIT_RATIO_LOW, threshold)
        else:
            return

    def _adviseLibraryCacheMissRatioHigh(self, startTime, endTime, threshold):
        stats = self._getLibraryCacheHitRatio(startTime, endTime)
        if stats is not None and stats[0] < 1 - threshold:
            return self._retrieveSuggestion(self._problemID.LIBRARY_CACHE_MISS_RATIO_HIGH, threshold)
        else:
            return

    def _adviseDataDictCacheMissRatioHigh(self, startTime, endTime, threshold):
        stats = self._getRowCacheHitRatio(startTime, endTime)
        if stats is not None and stats[0] < 1 - threshold:
            return self._retrieveSuggestion(self._problemID.DATA_DICTIONARY_CACHE_MISS_RATIO_HIGH,
                                            threshold)
        else:
            return

    def _adviseSharedPoolGrossUsageHigh(self, startTime, endTime, threshold):
        stats = self._getSharedPoolGrossUsagePct(startTime, endTime)
        if stats is not None and stats[0] > threshold:
            return self._retrieveSuggestion(self._problemID.SHARED_POOL_GROSS_USAGE_HIGH,
                                            threshold)
        else:
            return

    def _adviseHardParseRatioHigh(self, startTime, endTime, threshold):
        stats = self._getHardParseRatio(startTime, endTime)
        if stats is not None and stats[0] > threshold:
            return self._retrieveSuggestion(self._problemID.HARD_PARSE_RATIO_HIGH, threshold)
        else:
            return

    def _adviseRedoLogBufferSmall(self, startTime, endTime, threshold):
        stats = self._getRedoBufferHitRatio(startTime, endTime)
        if stats is not None and stats[0] > threshold:
            return self._retrieveSuggestion(self._problemID.REDO_LOG_BUFFER_SMALL, threshold)
        else:
            return

    def _adviseRedoLogBufferLarge(self, startTime, endTime, threshold):
        stats = self._getRedoBufferHitRatio(startTime, endTime)
        if stats is not None and stats[0] < threshold:
            return self._retrieveSuggestion(self._problemID.REDO_LOG_BUFFER_LARGE, threshold)
        else:
            return

    def _adviseSortAreaSmall(self, startTime, endTime, threshold):
        stats = self._getSortsDiskRatio(startTime, endTime)
        if stats is not None and stats[0] > threshold:
            return self._retrieveSuggestion(self._problemID.SORT_AREA_SMALL, threshold)
        else:
            return

    def _adviseSortAreaLarge(self, startTime, endTime, threshold):
        stats = self._getSortsDiskRatio(startTime, endTime)
        if stats is not None and stats[0] < threshold:
            return self._retrieveSuggestion(self._problemID.SORT_AREA_LARGE, threshold)
        else:
            return

    # functions to get data from stats database
    def _retrieveBlockEndPointDat(self, table, columns, startTime, endTime):
        # responsible for getting two row from database
        # the first row is the first row with update_time > startTime
        # the last row is the first row with update_time < endTime
        columnsStr = ', '.join(c for c in columns)
        sql = GET_FIRST_ROW_FOR_BLOCK_TEMPLATE.format(columns=columnsStr, table=table)
        startTimeStr = datetime.strftime(startTime, self._datetimeFmt)
        endTimeStr = datetime.strftime(endTime, self._datetimeFmt)
        firstRow = self._conn.execute(sql, (self._jobID, startTimeStr, endTimeStr)).fetchone()
        sql = GET_LAST_ROW_FOR_BLOCK_TEMPLATE.format(columns=columnsStr, table=table)
        lastRow = self._conn.execute(sql, (self._jobID, endTimeStr, startTimeStr)).fetchone()

        return firstRow, lastRow

    def _retrieveBlockAllDat(self, table, columns, startTime, endTime):
        # responsible for getting all data within the block
        # return all the rows got
        columnsStr = ', '.join(c for c in columns)
        sql = GET_DAT_BETWEEN_TIME_RANGE_TEMPLATE.format(columns=columnsStr, table=table)
        startTimeStr = datetime.strftime(startTime, self._datetimeFmt)
        endTimeStr = datetime.strftime(endTime, self._datetimeFmt)
        rows = self._conn.execute(sql, (self._jobID, startTimeStr, endTimeStr)).fetchall()

        return rows

    def _retrieveSuggestion(self, problemID, threshold):
        sugRow = self._conn.execute(GET_SUGGESTION_SQL, (problemID, )).fetchone()
        if sugRow is not None:
            suggestion = {column: sugRow[column] for column in sugRow.keys()}
            # format problem description
            suggestion['PROBLEM_DESCRIPTION'] = suggestion['PROBLEM_DESCRIPTION'].format(threshold * 100)
            return suggestion
        else:
            return None


if __name__ == '__main__':

    # TEST

    jobID = 1
    with Advisor(jobID) as adr:
        print adr.JobID
        print adr.JobStartTime()
        print adr.JobEndTime()

        startTime = datetime.strftime(adr.JobStartTime(), adr._datetimeFmt)
        endTime = datetime.strftime(adr.JobEndTime(), adr._datetimeFmt)
        interval = 120

        SID = StatsIdentifier()

        # ratios
        print adr.GetStats(SID.BUFFER_CACHE_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.LIBRARY_CACHE_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.ROW_CACHE_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.HARD_PARSE_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.REDO_BUFFER_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.SORTS_DISK_RATIO, startTime, endTime, interval)

        # average
        print adr.GetStats(SID.SHARED_POOL_GROSS_USAGE_PCT, startTime, endTime, interval)

        # test suggestions
        PID = ProblemIdentifier()
        print adr.GetSuggestion(PID.SORT_AREA_SMALL, startTime, endTime, -1)

        # test out of range time
        startTime = datetime.strftime(datetime.now(), adr._datetimeFmt)
        endTime = datetime.strftime(datetime.now() + timedelta(seconds=120), adr._datetimeFmt)

        print "out of range"
        print adr.GetStats(SID.BUFFER_CACHE_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.LIBRARY_CACHE_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.ROW_CACHE_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.HARD_PARSE_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.REDO_BUFFER_HIT_RATIO, startTime, endTime, interval)
        print adr.GetStats(SID.SORTS_DISK_RATIO, startTime, endTime, interval)

        # sql tuning
        print adr.GetLowEfficiencySql(datetime.now())
        print adr.GetLongRunSql(datetime.now())
        print adr.GetCPUIntensiveSession(datetime.now())
