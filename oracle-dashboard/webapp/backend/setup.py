__author__ = 'lihou'

# setup stats db

import sys
import sqlite3
import argparse
from Config import STATS_DB_FILENAME, SETUP_SCRIPT


def setupDB(conn, setupScript):
    print "Setting up stats database ... ",
    try:
        with open(setupScript) as f:
            scripts = f.read()
        conn.executescript(scripts)
        print "done"

    except IOError:
        print "failed"
        print "Unable to open setup script {}".format(setupScript)
    except:
        print "failed"
        raise

if __name__ == '__main__':

    #  parse argument
    parser = argparse.ArgumentParser(prog="Setup Statistic Database")
    parser.add_argument('--stats_db_name', default=STATS_DB_FILENAME)
    parser.add_argument('--setup_script', default=SETUP_SCRIPT)
    args = parser.parse_args()

    try:
        # open connection and cursor
        # if db not exists, will be automatically created
        conn = sqlite3.connect(args.stats_db_name)
    except sqlite3.OperationalError:
        print "Unable to open database file {}".format(STATS_DB_FILENAME)
        sys.exit()
    except:
        raise

    rollback = False
    try:
        setupDB(conn, args.setup_script)
    except:
        rollback = True
        raise
    finally:
        conn.rollback() if rollback else conn.commit()
        conn.close()
