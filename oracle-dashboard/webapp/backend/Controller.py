#!/usr/bin/env/python2

__author__ = 'lihou'

import os
import sys
import argparse
from time import sleep
from signal import SIGTERM
from subprocess import Popen

from Config import MONITOR_SCRIPT, PIDFILE, PID_LINE_STR_FORMAT,\
    EXIT_COULD_NOT_OPEN_PIDFILE, EXIT_CHILD_PROCESS_START_FAIL

PROCESS_START_TIME_TO_WAIT = 3  # wait 3 secs to check whether child process running
PROCESS_KILL_TIME_INTERVAL = 0.5
PID_IDX = 0  # pid index for tuple returned by monitors[jobid]


def getRunningMonitor(pidfile):
    # list running monitoring jobs in pidfile
    monitors = {}
    if os.path.isfile(pidfile):
        try:
            with open(pidfile, 'r') as pf:
                monitors = {int(jobid): (int(pid), tns, interval) for jobid, pid, tns, interval in map(str.split, pf)}
        except IOError:
            print "Could not read pidfile: {}".format(pidfile)
            sys.exit(EXIT_COULD_NOT_OPEN_PIDFILE)
        except:
            raise

    return monitors


def startMonitor(pidfile, jobid, user, passwd, tns, interval):
    # start monitor
    # NOT checking whether duplicated jobs are running
    args = [sys.executable, MONITOR_SCRIPT, jobid, user, passwd, tns, interval]
    try:
        p = Popen(args)  # start the monitor process

        # check process running
        sleep(PROCESS_START_TIME_TO_WAIT)
        if p.poll() is not None:
            print "Monitor[jid: {} pid: {}] starting failed due to exception.".format(jobid, p.pid)
            sys.exit(EXIT_CHILD_PROCESS_START_FAIL)
        else:
            print "Monitor[jid: {} pid: {}] for {} with interval {} started.".format(jobid, p.pid, tns, interval)

    except OSError:
        print "Could not start monitor"
        raise

    try:
        with open(pidfile, 'a+') as pf:
            pf.write(PID_LINE_STR_FORMAT.format(jobid, p.pid, tns, interval))
    except IOError:
        print "Could not write to pidfile: {}".format(pidfile)
        sys.exit(EXIT_COULD_NOT_OPEN_PIDFILE)


def removeJid(pidfile, jobid):
    # remove entry identified by jobid from pidfile
    # if pidfile empty, remove pidfile
    monitors = getRunningMonitor(pidfile)
    try:
        monitors.pop(jobid)
        if len(monitors):
            with open(pidfile, 'w+') as pf:
                pf.writelines((PID_LINE_STR_FORMAT.format(jobid, *info) for jobid, info in monitors.items()))
        else:
            os.remove(pidfile)
    except IOError:
        print "Could not write to pidfile: {}".format(pidfile)
        sys.exit(EXIT_COULD_NOT_OPEN_PIDFILE)
    except KeyError:
        # no entry with jobid found
        pass


def stopMonitor(pidfile, jobid):
    # stop monitor
    monitors = getRunningMonitor(pidfile)
    if jobid in monitors:
        pid = monitors[jobid][PID_IDX]
        try:
            while True:
                os.kill(pid, SIGTERM)
                sleep(PROCESS_KILL_TIME_INTERVAL)
        except OSError as err:
            if str(err).find('No such process'):
                removeJid(pidfile, jobid)
            else:
                raise
        print "Monitor[jid: {} pid: {}] stopped.".format(jobid, pid)
    else:
        print "No monitor with job id {} found".format(jobid)


if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--start', dest='jobInfo', nargs=5, metavar=('jobid', 'user', 'passwd', 'tns', 'interval'))
    group.add_argument('--stop', dest='jobid', type=int, metavar='jobid')
    group.add_argument('--list', action='store_true')

    parser.add_argument('--pidfile', default=PIDFILE, nargs=1, help='file to record running monitors')
    parser.add_argument('--machinereadable', action='store_true')
    args = parser.parse_args()

    if args.jobInfo is not None:
        startMonitor(args.pidfile, *args.jobInfo)

    elif args.jobid is not None:
        stopMonitor(args.pidfile, args.jobid)

    elif args.list:
        monitors = getRunningMonitor(args.pidfile)
        if not len(monitors):
            if args.machinereadable is None:
                print "No running monitors"
            if os.path.isfile(args.pidfile):
                os.remove(args.pidfile)
        else:
            for jobid, info in monitors.items():
                if args.machinereadable is None:
                    print "Process[jid: {} pid {}]: {} with interval {}".format(jobid, *info)
                else:
                    print "{}".format(jobid)
                

