#!/user/bin/env/python2
__author__ = 'lihou'

import signal
import sqlite3
import argparse
import threading

from os import getpid
from Queue import Queue, Empty
from datetime import datetime, timedelta
from traceback import print_tb
from cx_Oracle import InterfaceError, DatabaseError

from Config import STATS_DB_FILENAME, DEFAULT_STATS_GRAB_INTERVAL, MAX_QUEUE_SIZE, \
    QUEUE_WAIT_TIME, DEFAULT_QUEUE_TIMEOUT, SQLITE_DATETIME_FORMAT

from ConnectionGenerator import getDBconnectionGenerator  # get monitored db connection

from QueryPool import takeSnapshot, SQL_INSERT_TEMPLATE, COLLECT_STATS_SQL, \
    REGISTER_JOB_SQL, CHECK_JOB_ID_SQL, FINISH_JOB_SQL


class Collector(object):
    """
    Statistics collector, periodically collect stats from monitored db
    """

    def __init__(self, jobid, user, passwd, tns,
                 interval=DEFAULT_STATS_GRAB_INTERVAL,
                 takeSnapshotFunc=takeSnapshot):

        self._stop = threading.Event()
        self._collectionTimer = self._getNewTimer(interval)
        self._takeSnapshotFunc = takeSnapshotFunc

        self._jobID = jobid
        self._tns = tns
        self._interval = timedelta(seconds=interval)  # basic job info
        self._queueTimeout = DEFAULT_QUEUE_TIMEOUT if DEFAULT_QUEUE_TIMEOUT > interval \
            else interval + QUEUE_WAIT_TIME

        self._dbConnGen = getDBconnectionGenerator(user, passwd, tns)
        self._dbConn = None
        self._statsDBconn = None
        self._rollback = False

        self._queue = Queue(MAX_QUEUE_SIZE)

    def __enter__(self):
        self.openConnections()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        self._rollback = True if exc_type is not None else False
        self.closeConnections()
        self._collectionTimer.cancel()
        self.stop()

        if exc_type is not None:
            print_tb(exc_tb)
            raise exc_type(exc_val)

    def openConnections(self):
        # open db and stats db connections
        try:
            if self._dbConn is None:
                self._dbConn = self._dbConnGen.getNewConnection()
            self._dbConn.ping()
        except DatabaseError:
            print "could not connect to monitored database"
            raise
        except InterfaceError:
            self._dbConn = self._dbConnGen.getNewConnection()

        # TODO: ensure not opening a new connection if it is already open
        if self._statsDBconn is None:
            self._statsDBconn = sqlite3.connect(STATS_DB_FILENAME)  # open stats db conn

    def closeConnections(self):
        # close connections
        try:
            if self._dbConn is not None:
                self._dbConn.ping()
                self._dbConn.close()
        except InterfaceError:
            pass
        if self._statsDBconn is not None:
            self._statsDBconn.rollback() if self._rollback else self._statsDBconn.commit()
            self._statsDBconn.close()  # will not raise exception if already closed

    def run(self):
        # begin collection process
        print "Monitor[{}] starting".format(getpid())
        start = datetime.now()

        # open connection if not run with 'with'
        if self._dbConn is None or self._statsDBconn is None:
            self.openConnections()

        # register job
        self._registerJob(start)

        self._collectionTimer.start()  # start the thread to take snapshot of monitored db
        while True:
            if self._stop.is_set():
                # stop collector
                self._collectionTimer.cancel()
                print "Collection process in thread {} stopped.".format(threading.currentThread().getName())
                end = datetime.now()
                self._finishJob(end)

                mins = int((end - start).total_seconds() // 60)
                print "Collection running for {:4d} mins, {:4.2f} secs".format(
                    mins, (end - start).total_seconds() - 60 * mins)
                break
            else:
                # try get data to insert into stats db
                try:
                    snapshot = self._queue.get(block=True, timeout=self._queueTimeout)
                except Empty:
                    # TODO: not existing properly when exception occurs
                    if not self._collectionTimer.is_alive():
                        # exception may be raised in the timed collection thread
                        print "Stop collector due to timed collection dead."
                        self.stop()
                        continue
                    else:
                        raise

                # insert data to stats db
                self._insertSnapshot(snapshot)
                self._queue.task_done()

    def stop(self):
        self._stop.set()

    def _grabStats(self):
        """
        get statistics of monitored db at specified interval
        """
        startTime = datetime.now()

        # take stats snapshot from db
        currentSnapshot = self._takeSnapshot()
        # put snapshot into queue
        self._queue.put(currentSnapshot, block=True, timeout=self._queueTimeout)

        timeLeft = self._interval - (datetime.now() - startTime)
        timeLeft = timeLeft if timeLeft >= timedelta(seconds=0) else timedelta(seconds=0)
        self._collectionTimer = self._getNewTimer(timeLeft.total_seconds())
        self._collectionTimer.start()

    def _getNewTimer(self, interval):
        # return the next scheduled timer
        return threading.Timer(interval, self._grabStats)

    def _takeSnapshot(self):
        # a delegate function, pass a live connection to snapshot func
        try:
            self._dbConn.ping()
        except InterfaceError:
            self._dbConn = self._dbConnGen.getNewConnection()
        return self._takeSnapshotFunc(self._dbConn, COLLECT_STATS_SQL)

    def _insertSnapshot(self, snapshot):
        """
        insert to stats db
        Snapshot is a dict of which each entry is a pair as
        tableName: ((columns), (row_1), ..., (row_n))
        """
        for tableName, data in snapshot.items():
            sql = SQL_INSERT_TEMPLATE

            # construct ad-hoc sql from template
            sql = sql.replace('<tableName>', tableName)
            columns, rows = data[0], data[1:]
            columnsStr = ', '.join(c for c in columns)
            sql = sql.replace('<columns>', columnsStr)
            sql = sql.replace('<valuesPlaceHolder>', ', '.join(['?'] * (len(columns) + 1)))  # 1 for the job id

            # combine with jobID
            rowGen = ([self._jobID] + list(row) for row in rows)
            self._statsDBconn.executemany(sql, rowGen)
            self._statsDBconn.commit()

    def _registerJob(self, startTime):
        # check and register job
        existingJobID = self._statsDBconn.execute(CHECK_JOB_ID_SQL, (self._jobID, )).fetchone()
#        if existingJobID is not None:
#            raise ValueError("Duplicate Job ID: {} existing in db {}".format(self._jobID, STATS_DB_FILENAME))
        if existingJobID is None:
            self._statsDBconn.execute(REGISTER_JOB_SQL, (self._jobID, formatTime(startTime),
                                                         self._interval.total_seconds(),  self._tns))
            self._statsDBconn.commit()

    def _finishJob(self, end):
        # update job entry in stats db
        self._statsDBconn.execute(FINISH_JOB_SQL, (formatTime(end), self._jobID))
        self._statsDBconn.commit()


# helper functions
def formatTime(t, fmt=SQLITE_DATETIME_FORMAT):
    return datetime.strftime(t, fmt)


if __name__ == '__main__':
    # SIGTERM handler, stop the collector
    def stopCollection(signum, stack):
        global collector
        collector.stop()

    # parse argument
    parser = argparse.ArgumentParser()
    parser.add_argument('jobid', type=int, help='Job id for the monitoring job')
    parser.add_argument('user', help='User account to collect Oracle db')
    parser.add_argument('passwd', help='Password for the account')
    parser.add_argument('tns', help='Oracle instance tns')
    parser.add_argument('interval', type=int,
                        help='Statistics grab interval in second')
    args = parser.parse_args()

    # register SIGTERM handler

    signal.signal(signal.SIGTERM, stopCollection)

    collector = Collector(**vars(args))

    with collector:
        collector.run()

