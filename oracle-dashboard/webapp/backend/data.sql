-- populate SUGGESTIONS table
INSERT INTO SUGGESTIONS(PROBLEM_ID, PROBLEM_DESCRIPTION, SOLUTION, INIT_PARAMETER, REFERENCE, QUERY )
      SELECT 1 AS 'PROBLEM_ID',
             'Libray cache miss ratio is over {:.1f}%' AS 'PROBLEM_DESCRIPTION',
             'You should consider adding to the shared_pool_size.' AS 'SOLUTION',
             'shared_pool_size' AS 'INIT_PARAMETER',
             'http://www.dba-oracle.com/concepts/shared_pool_concepts.htm' AS 'REFERENCE',
             NULL AS 'QUERY'
UNION SELECT 2, 'Data dictionary cache miss ratio exceeds {:.1f}%',
             'You should consider adding to the shared_pool_size.', 'shared_pool_size',
             'http://www.dba-oracle.com/concepts/shared_pool_concepts.htm', NULL
UNION SELECT 3, 'Gross usage of the shared pool in a non-ad-hoc environment exceeds {:.1f}%',
             'Gradually increase shared pool by 20% increments until usage drops below 90% on the average.',
             'shared_pool_size', 'http://www.dba-oracle.com/concepts/shared_pool_concepts.htm', NULL
UNION SELECT 4, 'Hard parsing ratio exceeds {:.1f}%',
             '1. you may consider adding to the shared_pool_size.
              2. you may want to change cursor_sharing to SIMILAR if the problem is due to not binding parameters in SQL', 'shared_pool_size',
             'http://www.dba-oracle.com/t_hard_vs_soft_parse_parsing.htm', NULL
UNION SELECT 5, 'Buffer cache hit ratio is less than {:.1f}%',
             'you can increase the size of database block buffers to increase the database performance. You may have to increase the physical memory on the server if the server starts swapping after increasing block buffers.',
             'db_cache_size', 'reference = "http://www.oracleskill.com/oracle-world-wide-article/tuning-database-buffer-cache-oracle.html',
             NULL
UNION SELECT 6, 'Buffer entries HIT Ratio is over {:.1f}%',
             'You can increase redo log buffer size by setting log_buffer in init.ora. A value of 10MB for the log buffer is a reasonable value for Oracle Applications and it represents a balance between concurrent programs and online users. The value of log_buffer must be a multiple of redo block size.',
             'log_buffer', 'http://www.dba-oracle.com/t_log_buffer_optimal_size.htm', NULL
UNION SELECT 7, 'Redo log buffer is too large. This may cause the LGWR process to write more data less often, causing yet another log-related Oracle wait event',
             'You can decrease redo log buffer size.', 'log_buffer',
             'http://www.confio.com/logicalread/oracle-log-buffer-space-wait-event/#.UVj8chzmuz0', NULL
UNION SELECT 8, 'Sort(disk) / (Sort(disk) + Sort(memory)) is over {:.1f}%',
             '1. increase memory for sorting.
              2. add some indexes to avoid unnecessary sorting.',
             'sort_area_size', 'http://www.praetoriate.com/t_ault1_84_tuning_sorts_information.htm', NULL
UNION SELECT 9, 'Sort(disk) / (Sort(disk) + Sort(memory)) is below {:.1f}%',
             '1. decrease memory for sorting.', 'sort_area_size',
             'http://www.praetoriate.com/t_ault1_84_tuning_sorts_information.htm', NULL
;
