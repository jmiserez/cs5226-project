__author__ = 'lihou'

import cx_Oracle as cx


class _dbConnectionGenerator(object):
    def __init__(self, user, passwd, tns):
        self._user = user
        self._passwd = passwd
        self._tns = tns

    def getNewConnection(self):
        """
        get a new connection of monitored database
        """
        return cx.connect(self._user, self._passwd, self._tns)


def getDBconnectionGenerator(user, passwd, tns):
    """
    return a db connection generator 
    """
    return _dbConnectionGenerator(user, passwd, tns)
