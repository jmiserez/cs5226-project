__author__ = 'lihou'


DIRECT_CALL_PATH_PREFIX = './backend/'

# pidfile
PIDFILE = 'monitors.pid'

# stats database
STATS_DB_FILE = 'stats.db'
SETUP_SCRIPT = 'setup.sql'

# monitor
MONITOR_SCRIPT = 'Monitor.py'
PID_LINE_STR_FORMAT = '{} {} {} {}\n'  # jobid, pid, tns, interval

# the following datetime format should return the same date string
ORACLE_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'
SQLITE_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


# exit status
EXIT_COULD_NOT_OPEN_PIDFILE = 1
EXIT_CHILD_PROCESS_START_FAIL = 2

# stats database info
STATS_DB_FILENAME = 'stats.db'

# statistics collector
DEFAULT_STATS_GRAB_INTERVAL = 30
MAX_QUEUE_SIZE = 100
DEFAULT_QUEUE_TIMEOUT = 60  # this should be at least greater than STATS_GRAB_INTERVAL
QUEUE_WAIT_TIME = 30

# collection queries specified
SQL_TUNING_RETURN_LIMIT = 10
