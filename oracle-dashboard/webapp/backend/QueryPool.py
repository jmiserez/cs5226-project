#!/usr/env/python2
# __author__ = 'lihou'

from Config import ORACLE_TIMESTAMP_FORMAT, SQL_TUNING_RETURN_LIMIT

# All the queries uses in the collection stats process

# SQL template
# SQL insert command template for stats db
SQL_INSERT_TEMPLATE = 'INSERT INTO <tableName>(JOB_ID, <columns>) values (<valuesPlaceHolder>)'


# COLLECTION queries
# BUFFER POOL
# pivot three columns
GET_BUFFER_POOL_HIT_RATIO = '''
        SELECT UPDATE_TIME,
               MAX(DECODE(NAME, 'db block gets from cache', VALUE, NULL)) DB_BLOCK_GETS_FROM_CACHE,
               MAX(DECODE(NAME, 'consistent gets from cache', VALUE, NULL)) CONSISTENT_GETS_FROM_CACHE,
               MAX(DECODE(NAME, 'physical reads cache', VALUE, NULL)) PHYSICAL_READS_CACHE
        FROM (
            SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME, NAME, VALUE
            FROM V$SYSSTAT
            WHERE NAME IN ('db block gets from cache',
                           'consistent gets from cache',
                           'physical reads cache')
            )
        GROUP BY UPDATE_TIME
        '''.format(ORACLE_TIMESTAMP_FORMAT)

# SHARED POOL
GET_LIBRARY_CACHE = '''
    SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
        SUM(PINS) PINS_SUM,
        SUM(PINHITS) PINHITS_SUM
    FROM
        V$LIBRARYCACHE
    '''.format(ORACLE_TIMESTAMP_FORMAT)

# row cache
GET_ROW_CACHE = '''
    SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
           SUM(FIXED)     FIXED_SUM,
           SUM(GETS)      GETS_SUM,
           SUM(GETMISSES) GETMISSES_SUM
    FROM
        V$ROWCACHE
    '''.format(ORACLE_TIMESTAMP_FORMAT)

# hard parse
GET_HARD_SOFT_PARSE = '''
    SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
           MAX(DECODE(name, 'parse count (hard)', value, NULL)) PARSE_COUNT_HARD,
       	   MAX(DECODE(name, 'parse count (total)', value, NULL)) PARSE_COUNT_TOTAL
    FROM V$SESSTAT NATURAL JOIN V$STATNAME
    WHERE sid = sys_context('userenv', 'sid')
      AND name IN ('parse count (total)', 'parse count (hard)')
    '''.format(ORACLE_TIMESTAMP_FORMAT)

GET_SHARED_POOL_GROSS_USAGE = '''
    SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
           SUM(a.bytes) SHARED_POOL_USED,
           MAX(b.value) SHARED_POOL_SIZE
    FROM V$SGASTAT a, V$PARAMETER b
    WHERE (a.POOL = 'shared pool' and a.NAME != 'free memory')
        AND b.NAME = 'shared_pool_size'
'''.format(ORACLE_TIMESTAMP_FORMAT)

# REDO LOG
GET_REDO_LOG_STAT = '''
    SELECT A.UPDATE_TIME, A.REDO_ENTRIES, A.REDO_BUFFER_ALLOCATION_RETRIES,
           B.BYTES LOG_FILESIZE, C.OPTIMAL_LOGFILE_SIZE
    FROM (
        SELECT UPDATE_TIME,
                   MAX(DECODE(NAME, 'redo entries', VALUE, NULL)) REDO_ENTRIES,
                   MAX(DECODE(NAME, 'redo buffer allocation retries', VALUE, NULL)) REDO_BUFFER_ALLOCATION_RETRIES
            FROM (
                SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME, NAME, VALUE
                FROM V$STATNAME NATURAL JOIN V$SYSSTAT
                WHERE NAME IN ('redo entries',
                               'redo buffer allocation retries')
                )
            GROUP BY UPDATE_TIME
    ) A, V$LOG B, V$INSTANCE_RECOVERY C
    WHERE B.STATUS = 'CURRENT'
    '''.format(ORACLE_TIMESTAMP_FORMAT)

# MEMORY FOR SORTING
GET_SORTS_MEM_STAT = '''
    SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
           SIZ.VALUE SORT_AREA_SIZE,
           RSIZ.VALUE SORT_AREA_RETAINED_SIZE,
           MEM.VALUE SORTS_MEMORY,
           DSK.VALUE SORTS_DISK
    FROM
        V$SYSSTAT MEM, V$SYSSTAT DSK, V$PARAMETER SIZ, V$PARAMETER RSIZ
    WHERE
        MEM.NAME = 'sorts (memory)'
        AND DSK.NAME = 'sorts (disk)'
        AND SIZ.NAME = 'sort_area_size'
        AND RSIZ.NAME = 'sort_area_retained_size'
    '''.format(ORACLE_TIMESTAMP_FORMAT)

# PROBLEMATIC SQL
GET_LOW_EFFICIENCY_SQL = '''
    SELECT *
    FROM (
        SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
            s.SID,
            b.SPID,
            s.SQL_HASH_VALUE,
            q.SQL_TEXT,
            q.EXECUTIONS,
            q.BUFFER_GETS,
            ROUND(q.buffer_gets / q.executions) AS BUFFER_PER_EXEC,
            ROUND(q.elapsed_time / q.executions) AS CPU_TIME_PER_EXEC,
            q.CPU_TIME,
            q.ELAPSED_TIME,
            q.DISK_READS,
            q.ROWS_PROCESSED
        FROM V$SESSION s, V$PROCESS b, V$SQL q
        WHERE s.sql_hash_value = q.hash_value
        AND s.paddr = b.addr
        AND s.status = 'ACTIVE'
        AND s.TYPE = 'USER'
        AND q.buffer_gets > 0
        AND q.executions > 0
        ORDER BY buffer_per_exec DESC)
     WHERE ROWNUM <= {rownum}
'''.format(ORACLE_TIMESTAMP_FORMAT, rownum=SQL_TUNING_RETURN_LIMIT)

GET_LONG_RUN_SQL = '''
    SELECT * FROM
    (
        SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
            SID,
            OPNAME,
            TARGET,
            SOFAR,
            TOTALWORK,
            UNITS,
            START_TIME,
            LAST_UPDATE_TIME,
            TIME_REMAINING,
            ELAPSED_SECONDS,
            MESSAGE,
            USERNAME
        FROM
            V$SESSION_LONGOPS
        ORDER BY TIME_REMAINING DESC
    )
    WHERE ROWNUM < {rownum}
'''.format(ORACLE_TIMESTAMP_FORMAT, rownum=SQL_TUNING_RETURN_LIMIT)

GET_CPU_INTENSIVE_SESSION = '''
    SELECT TO_CHAR(LOCALTIMESTAMP, '{}') UPDATE_TIME,
        a.SID,
        SPID,
        STATUS,
        substr(a.program,1,40) PROG,
        a.TERMINAL,
        OSUSER,
        value/60/100 VALUE
    FROM v$session a,v$process b,v$sesstat c
    WHERE c.statistic#=12 and c.sid=a.sid and a.paddr=b.addr order by value desc
    '''.format(ORACLE_TIMESTAMP_FORMAT)

# ALL COLLECTION QUERIES
# all data needed for each snapshot is collected by the following queries
COLLECT_STATS_SQL = dict(
    # buffer pool
    BUFFER_POOL_HIT_RATIO=GET_BUFFER_POOL_HIT_RATIO,
    # shared pool
    LIBRARY_CACHE=GET_LIBRARY_CACHE,
    ROW_CACHE=GET_ROW_CACHE,
    HARD_SOFT_PARSE=GET_HARD_SOFT_PARSE,
    SHARED_POOL_GROSS_USAGE=GET_SHARED_POOL_GROSS_USAGE,
    # redo log
    REDO_LOG_STAT=GET_REDO_LOG_STAT,
    # sort memory
    SORTS_MEM_STAT=GET_SORTS_MEM_STAT,
    # problematic sql
    LOW_EFFICIENCY_SQL=GET_LOW_EFFICIENCY_SQL,
    LONG_RUN_SQL=GET_LONG_RUN_SQL,
    # cpu monitoring
    CPU_INTENSIVE_SESSION=GET_CPU_INTENSIVE_SESSION
)


# JOBS RELATED
REGISTER_JOB_SQL = 'INSERT INTO JOB_REGISTRY(JOB_ID, START_TIME, INTERVAL, TNS) VALUES (?, ?, ?, ?)'
CHECK_JOB_ID_SQL = 'SELECT JOB_ID FROM JOB_REGISTRY WHERE JOB_ID = ?'
FINISH_JOB_SQL = 'UPDATE JOB_REGISTRY SET END_TIME = ? WHERE JOB_ID = ?'


# used for collection process
def takeSnapshot(conn, queries):
    snapshot = {}
    cur = conn.cursor()
    try:
        for tableName, sql in queries.items():
            stats = takeTableSnapshot(cur, tableName, sql)
            snapshot.update(stats)
    finally:
        cur.close()
    return snapshot


def prepareResults(cur):
    # prepare results to the format used to insert
    headers = tuple(desc[0] for desc in cur.description)
    values = cur.fetchall()
    return headers, values


def takeTableSnapshot(cur, tableName, sql):
    # take a cur to execute sql passed
    res = {}
    cur.execute(sql)
    headers, values = prepareResults(cur)
    if cur.rowcount > 0:
        res[tableName] = []
        res[tableName].append(headers)
        res[tableName].extend(values)
    return res

